import Container from './containers/container.jsx';
import React from 'react';
import ReactDom from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Router, Route, Link, IndexRoute, hashHistory, browserHistory, DefaultRoute } from 'react-router'

import Booking from './containers/booking.jsx';
import DetailPage from './containers/detail.jsx';
import Experiment from './containers/experiment.jsx';
import LogoutView from './components/logout.jsx';
import Account from './containers/account.jsx';
import Facebook from './containers/facebook.jsx';
import Google from './containers/google.jsx';
import Design from './containers/designLogin.jsx';
import Base from './components/Base.jsx';
import HomePage from './components/HomePage.jsx';
import Home from './components/HomePage.jsx';
import DashboardPageNew from './containers/DashboardPageNew.jsx';
import DashboardPage from './containers/DashboardPage.jsx';
import LoginPage from './containers/LoginPage.jsx';
import SignUpPage from './containers/SignUpPage.jsx';
import hostSignUpPage from './containers/hostSignUpPage.jsx';
import SearchPage from './containers/SearchPage.jsx';
import ProfilePage from './containers/profilePage.jsx';
import BookingPage from './containers/bookingPage.jsx';
import HistoryPage from './containers/historyPage.jsx';
import StripePage from './containers/stripePage.jsx';
import PaypalPage from './containers/paypalPage.jsx';
import history from 'history';

import SimpleMap from './containers/demoMap.jsx';
import StorePage from './containers/StorePage.jsx';
import Tab from './containers/tabs.jsx';
import addstorePage from './components/addstore.jsx';
import editstorePage from './components/editstore.jsx';
import Parent from './containers/loginNew.jsx';
import SimpleMapPage from './containers/simple_map_page.jsx';
import Demo from './containers/demo1.jsx';
//import MainMapPage from './containers/main_map_page.jsx';
import GooglePage from './containers/GooglePage.jsx';
import Auth from './modules/Auth';



const routes = {
  // base component (wrapper for the whole application).
  component: Base,
  childRoutes: [

    {
      path: '/dashboard',
      getComponent: (location, callback) => {
        if (Auth.isUserAuthenticated()) {
			//console.log("token_______"+token);
          callback(null, DashboardPage);
        } else {
          callback(null, HomePage);
        }
      }
    },
	
    {
      path: '/dashboardnew',
	  component: DashboardPageNew
     
    },
	{
      path: '/',
      component: HomePage
    },

    {
      path: '/login',
      component: LoginPage
    },

    {
      path: '/signup',
      component: SignUpPage
    },
    {
      path: '/search',
      component: Container
    },
	{
      path: '/profile',
      component: ProfilePage
    },
	
	{
      path: '/history',
      component: HistoryPage
    },
	{
      path: '/stripe',
      component: StripePage
    },
	{
      path: '/paypal',
      component: PaypalPage
    },
	{
      path: '/store',
      component: StorePage
    },
	{
      path: '/addstore',
      component: addstorePage
    },
	{
      path: '/editstore',
      component: editstorePage
    },
	{
      path: '/hostsignup',
      component: hostSignUpPage
    },
	{
      path: '/demo',
      component: Demo
    },
	{
      path: '/parent',
      component: Parent
    },
	{
      path: '/designLogin',
      component: Design
    },
	{
      path: '/facebook',
      component: Facebook
    },
	{
      path: '/google',
      component: Google
    },
	{
      path: '/experiment',
      component: Experiment
    },
	
	{
      path: '/userdata'
    },
   	{
      path: '/account',
	  component: Account
    },
		{
      path: '/logout',
	  component: LogoutView
    },
		{
      path: '/detail',
	  component: DetailPage
    },
	{
      path: '/booking',
	  component: Booking
    },
	{
      path: '/demoMap',
      component: SimpleMap
    },
	 
	 
	

  ]
};


export default routes;
