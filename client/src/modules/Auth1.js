class Auth1 {

  /**
   * Authenticate a user. Save a token string in Local Storage
   *
   * @param {string} token
   */
  static authenticateUser(response.id) {
    localStorage.setItem('response.id', response.id);
    //localStorage.setItem('user', user);
	console.log("token is... "+response.id);
  }

  /**
   * Check if a user is authenticated - check if a token is saved in Local Storage
   *
   * @returns {boolean}
   */
 
   
  static isUserAuthenticated() {
    return localStorage.getItem('response.id') !== null;
  }
  
  /*static getData(){
	  return localStorage.getItem('user');
  }*/

  /**
   * Deauthenticate a user. Remove a token from Local Storage.
   *
   */
  static deauthenticateUser() {
    localStorage.removeItem('response.id');
  }

  /**
   * Get a token value.
   *
   * @returns {string}
   */

  static getToken() {
    return localStorage.getItem('response.id');
  }

}

export default Auth1;
