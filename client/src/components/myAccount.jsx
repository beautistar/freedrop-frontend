
import { Card, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
//import MonthPicker from './MonthPicker.jsx';
import Modal from 'react-modal';
var React = require('react');
import TimePicker from 'material-ui/TimePicker';
import DatePicker from 'material-ui/DatePicker';
var ReactScriptLoaderMixin = require('react-script-loader').ReactScriptLoaderMixin;

var MyAccount = React.createClass({
  getInitialState: function() {
    return {
      
      submitDisabled: false,
      modalIsOpen: false,
	  dateTime: null,
	  date: null,
    time: null,
    };
	 this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDate = this.handleDate.bind(this);
    this.handleTime = this.handleTime.bind(this);
	this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.disableRandomDates = this.disableRandomDates.bind(this);
	
  },
  
   showTimePicker: function() {
    this.refs.timepicker.openDialog();
  },

  openModal: function() {
  //openModal() {
    this.setState({modalIsOpen: true});
  },
 
 afterOpenModal: function() {
  },
 closeModal: function() {
  //closeModal() {
    this.setState({modalIsOpen: false});
  },
  
  disableRandomDates: function() {
  return Math.random() > 0.7;
}, 
  
  render: function() {
      return ( 	<div>
	
			<form onSubmit={this.onSubmit} >
			
		
			
			  
			 
			  <div className="white field-line1">Personal Details:</div>
			  
			  	<div className="field-line1">
				<TextField
				  floatingLabelText="Full Name"
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  <div className="field-line1">
				<TextField
				  floatingLabelText="Home City"
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  <div className="field-line1">
				<TextField
				  floatingLabelText="Country"
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  <div className="field-line1">
				<TextField
				  floatingLabelText="Mobile No."
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  	 <div className="field-line1">
				<DatePicker
				  
				  floatingLabelText="Date of Birth"
						  inputStyle={{color: 'white'}}
						  floatingLabelStyle={{color: 'white' }}
						  
				/>
				
      </div>
			   <div className="field-line1">
				<TextField
				  floatingLabelText="Email Address"
				  name="email"
				  data-stripe='cvc'  
				inputStyle={{color: 'white' }}	
				floatingLabelStyle={{color: 'white'}}				
				/>
			  </div>
			  
				<div className="button-line1">
				<RaisedButton disabled={this.state.submitDisabled} type="button" label="Save" primary />
			  </div>
			  </form>
			  </div>
			
	  
	  );
    
  }
});


module.exports = MyAccount;
