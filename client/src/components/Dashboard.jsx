import React, { PropTypes } from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import LoginPage from '../containers/LoginPage.jsx';


const Dashboard = ({ secretData }) => (

  <Card className="container">
    <CardTitle
	  className="card-heading"
      title="Dashboard"
      subtitle="You should get access to this page only after authentication."
    />

    {secretData && <CardText style={{ fontSize: '16px', color: 'green' }}>{secretData}</CardText>}
	
		
  </Card>
  
);

Dashboard.propTypes = {
  secretData: PropTypes.string.isRequired,
  //userdata: PropTypes.string.isRequired
};

export default Dashboard;
