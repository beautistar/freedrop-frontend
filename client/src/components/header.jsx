import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import { hashHistory } from 'react-router';
import Auth from '../modules/Auth';
 const headerstyle= {
				backgroundColor: '#52d3aa',
				marginTop: '0',
				//backgroundImage:'url(images/navbar.jpg)',
				backgroundImage: '-webkit-gradient(linear, 0% 0%, 100% 100%, color-stop(0, #3f95ea), color-stop(1, #52d3aa))',
				backgroundImage: '-webkit-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)',
				backgroundImage: 'repeating-linear-gradient(to bottom right, #3f95ea 0%, #52d3aa 100%)',
				backgroundImage: '-ms-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)'
			}
const tagPath={
	} 
	
const external = "external"
class Header extends React.Component {	

  constructor(props, context) {
    super(props, context);
	
  }
  
  
  
  serch_page(){
		
			window.location.href="http://"+location.host+'/#/search';

			location.reload()
	}
   render() {
    return (
    
     
	  <header role="banner" className={this.props.className} id="fh5co-header" style={this.props.style}>
		  {/*<header role="banner" className={"http://"+location.host+'/#/search'== location.href?'headerstylesdsd':'' } id="fh5co-header" style={"http://"+location.host+'/#/'!= location.href?headerstyle:tagPath }>*/}
	 
                            <div className="container">	                       
                        
                                <nav className="navbar navbar-default">
									
                                    <div className="navbar-header">
									
                                    <a href="#" className="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                                    {this.props.children}
									<a className="navbar-brand" href="/">FreeDrop</a> 
									
                                    </div>
                                    <div id="navbar" className="navbar-collapse collapse">
                                    <ul className="nav navbar-nav navbar-right">
                                        
	 <li> <Link  className="external" onClick={this.serch_page} style={{cursor:'pointer'}}><span>Search</span></Link> </li>
	
	
	{Auth.isUserAuthenticated() ?<li> <Link  className="external" to="/dashboard"><span>Dashboard</span></Link> </li>:''}
{Auth.isUserAuthenticated() ? <li><Link className="external" to="/addstore">Add Store</Link> </li>: ''} 
{Auth.isUserAuthenticated() ? <li><Link className="external" to="/editstore">Edit Store</Link> </li>: ''} 
									  {Auth.isUserAuthenticated() ?     
         <li><Link className="external" to="/logout">Log out</Link> </li> 
		 
      : (   
			<li className="dropdown">
			<button className="dropbtn"><Link className="external">Log in/Register</Link></button>
			<ul className="dropdown-content"  style={"http://"+location.host+'/#/'!= location.href?headerstyle:tagPath }>
			<Link className="external" to="/parent">User</Link>
			<Link className="external" to="/store">Host</Link>
			</ul>
			</li>
		
	)}
	 </ul>                                   
                                    </div>                                    
                                </nav>                                              
                        </div>
						 
                    </header> 

    

  
   )}
  }
  
  

  
  

Header.propTypes = {
 className: PropTypes.string,
 id: PropTypes.string,
 style: PropTypes.object,
 children: PropTypes.object
};


export default Header;
