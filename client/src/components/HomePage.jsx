import React from 'react';
import { Card, CardTitle } from 'material-ui/Card';
import Modal from 'react-modal';
import { Link, IndexLink } from 'react-router';
import Header from './header.jsx';
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
	width: '620px',
    height: '450px'
  }
};

//const HomePage = () => ({
class HomePage extends React.Component {
 constructor() {
    super();
 
    this.state = {
      modalIsOpen: false
    };
 
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
   serch_page(){
		
			window.location.href="http://"+location.host+'/#/search';

			location.reload()
	}
 
  openModal() {
    this.setState({modalIsOpen: true});
  }
 
  afterOpenModal() {
    // references are now sync'd and can be accessed. 
    //this.subtitle.style.color = '#f00';
  }
 
  closeModal() {
    this.setState({modalIsOpen: false});
  }
	
	
	
	
//const HomePage = () => (
render() {
	return (
<div>
{ /* <Card className="container">
    <CardTitle title="React Applicationnn" subtitle="This is the home page." />
  </Card>*/}
  <Header />
   	                  
	<div>

	<section id="fh5co-home" data-section="home" style={{ backgroundImage: 'url(images/full_image_2.jpg)' }} data-stellar-background-ratio="0.5">
		<div  className="gradient"></div>
		<div  className="container">
			<div  className="text-wrap">
				<div  className="text-inner">
					<div  className="row">
					<div className="container">
	<div className="row">
        <div className="col-sm-6 col-sm-offset-3">
            <div id="imaginary_container"> 
                <div className="input-group stylish-input-group">
                    <input type="text" className="external form-control"  placeholder="Search" />
                    <span className="input-group-addon">
                        <button type="submit">
                            <span className="glyphicon glyphicon-search" onClick={this.openModal}></span>
                        </button>  
                    </span>
                </div>
            </div>
        </div>
	</div>
</div>





 
   
		 <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
		<div className="row">
			<table className="table">
			 <thead>
      <tr>
        <th>
		<div ><img src="images/m1.png" style={{maxWidth: '15%'}} alt="Image"  className="img-responsive"  />
		<Link  className="external" onClick={this.serch_page} style={{cursor:'pointer'}}><span>Search from current location</span></Link>
		</div></th>
		<td><button type="button" className="close" aria-label="Close" style={{fontSize:'50px'}} onClick={this.closeModal}>
  <span aria-hidden="true">&times;</span>
</button> </td>
		
		
      </tr>
    </thead>
	
    <thead>
      <tr>
        <th>Osawa</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
		</div>
		<div className="row">
			<table className="table">
    <thead>
      <tr>
        <th>Kyoto</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
		</div>
		<div className="row">
			<table className="table">
    <thead>
      <tr>
        <th>Tokyo</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
		</div>
 
          <h2 ref={subtitle => this.subtitle = subtitle}>Hello</h2>
          
         
		 
</Modal>
  










					
						<div  className="col-md-8 col-md-offset-2">
			
							<h1  className="to-animate">Do something you love.</h1>
							<h2  className="to-animate">Another free HTML5 bootstrap template by <a href="http://freehtml5.co/" target="_blank" title="Free HTML5 Bootstrap Templates">FREEHTML5.co</a> released under <a href="http://creativecommons.org/licenses/by/3.0/" target="_blank">Creative Commons 3.0</a></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div  className="slant"></div>
		
	</section>

	<section id="fh5co-intro">
		<div  className="container">
			<div  className="row row-bottom-padded-lg">
				<div  className="fh5co-block to-animate" style={{ backgroundImage: 'url(images/img_7.jpg)' }}>
					<div  className="overlay-darker"></div>
					<div  className="overlay"></div>
					<div  className="fh5co-text">
						<i  className="fh5co-intro-icon icon-bulb"></i>
						<h2>Plan</h2>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						<p><a href="#"  className="btn btn-primary">Get In Touch</a></p>
				
					</div>
				</div>
				<div  className="fh5co-block to-animate" style={{ backgroundImage: 'url(images/img_8.jpg)'}}>
					<div  className="overlay-darker"></div>
					<div  className="overlay"></div>
					<div  className="fh5co-text">
						<i  className="fh5co-intro-icon icon-wrench"></i>
						<h2>Develop</h2>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						<p><a href="#"  className="btn btn-primary">Click Me</a></p>
					</div>
				</div>
				<div  className="fh5co-block to-animate" style={{ backgroundImage: 'url(images/img_10.jpg)'}}>
					<div  className="overlay-darker"></div>
					<div  className="overlay"></div>
					<div  className="fh5co-text">
						<i  className="fh5co-intro-icon icon-rocket"></i>
						<h2>Launch</h2>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						<p><a href="#"  className="btn btn-primary">Why Us?</a></p>
					</div>
				</div>
			</div>
			<div  className="row watch-video text-center to-animate">
				<span>Watch the video</span>

				<a href="https://vimeo.com/channels/staffpicks/93951774"  className="popup-vimeo btn-video"><i  className="icon-play2"></i></a>
			</div>
		</div>
	</section>

	<section id="fh5co-work" data-section="work" >
		<div  className="container">
			<div  className="row">
				<div  className="col-md-12 section-heading text-center">
					<h2  className="to-animate">How it Works</h2>
					<div  className="row">
						<div  className="col-md-8 col-md-offset-2 subtext to-animate">
							<h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</h3>
						</div>
					</div>
				</div>
			</div>
			<div  className="row row-bottom-padded-sm">
				<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/step_1.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/step_1.jpg" alt="Image"  className="img-responsive"  />
						<div  className="fh5co-text">
						<h2>Step 1</h2>
						<span>Search Space</span>
						</div>
					</a>
				</div>
				<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/step_21.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/step_21.jpg" alt="Image"  className="img-responsive"  />
						<div  className="fh5co-text">
						<h2>Step 2</h2>
						<span>Create account</span>
						</div>
					</a>
				</div>

				<div  className="clearfix visible-sm-block"></div>

				<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/step_2.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/step_2.jpg" alt="Image"  className="img-responsive" />
						<div  className="fh5co-text">
						<h2>Step 3</h2>
						<span>Reserve Space</span>
						</div>
					</a>
				</div>
				<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/step_3.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/step_3.jpg" alt="Image"  className="img-responsive" />
						<div  className="fh5co-text">
						<h2>Step 4</h2>
						<span>Store Your Luggage</span>
						</div>
					</a>
				</div>
				
				<div  className="clearfix visible-sm-block"></div>

				<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/step_5.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/step_5.jpg" alt="Image"  className="img-responsive" />
						<div  className="fh5co-text">
						<h2>Step 5</h2>
						<span>Make Payment</span>
						</div>
					</a>
				</div>
				<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/step_4.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/step_4.jpg" alt="Image"  className="img-responsive" />
						<div  className="fh5co-text">
						<h2>Step 6</h2>
						<span>Pick up Your Luggage</span>
						</div>
					</a>
				</div>
				
				<div  className="clearfix visible-sm-block"></div>

				{/*<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/work_7.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/work_7.jpg" alt="Image"  className="img-responsive" />
						<div  className="fh5co-text">
						<h2>Project 3</h2>
						<span>Store Your Luggage</span>
						</div>
					</a>
				</div>
				<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/work_8.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/work_8.jpg" alt="Image"  className="img-responsive" />
						<div  className="fh5co-text">
						<h2>Project 4</h2>
						<span>Sketch</span>
						</div>
					</a>
				</div>

				<div  className="clearfix visible-sm-block"></div>

				<div  className="col-md-4 col-sm-6 col-xxs-12">
					<a href="images/work_1.jpg"  className="fh5co-project-item image-popup to-animate">
						<img src="images/work_1.jpg" alt="Image"  className="img-responsive" />
						<div  className="fh5co-text">
						<h2>Project 2</h2>
						<span>Illustration</span>
						</div>
					</a>
</div>*/}
			</div>
			<div  className="row">
				<div  className="col-md-12 text-center to-animate">
					<p>* Demo images from <a href="http://plmd.me/" target="_blank">plmd.me</a></p>
				</div>
			</div>
		</div>
	</section>

	<section id="fh5co-testimonials" data-section="testimonials" >
		<div  className="container">
			<div  className="row">
				<div  className="col-md-12 section-heading text-center">
					<h2  className="to-animate">Testimonials</h2>
					<div  className="row">
						<div  className="col-md-8 col-md-offset-2 subtext to-animate">
							<h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</h3>
						</div>
					</div>
				</div>
			</div>
			<div  className="row">
				<div  className="col-md-4">
					<div  className="box-testimony">
						<blockquote  className="to-animate-2">
							<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
						</blockquote>
						<div  className="author to-animate">
							<figure><img src="images/person3.jpg" alt="Person"/></figure>
							<p>
							Jean Doe, CEO <a href="http://freehtml5.co/" target="_blank">FREEHTML5.co</a> <span  className="subtext">Creative Director</span>
							</p>
						</div>
					</div>
				</div>
				<div  className="col-md-4">
					<div  className="box-testimony">
						<blockquote  className="to-animate-2">
							<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&rdquo;</p>
						</blockquote>
						<div  className="author to-animate">
							<figure><img src="images/person3.jpg" alt="Person"/></figure>
							<p>
							John Doe, Senior UI <a href="http://freehtml5.co/" target="_blank">FREEHTML5.co</a> <span  className="subtext">Creative Director</span>
							</p>
						</div>
					</div>
				</div>

				<div  className="col-md-4">
					<div  className="box-testimony">
						<blockquote  className="to-animate-2">
							<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. &rdquo;</p>
						</blockquote>
						<div  className="author to-animate">
							<figure><img src="images/person3.jpg" alt="Person" /></figure>
							<p>
							Chris Nash, Director <a href="http://freehtml5.co/" target="_blank">FREEHTML5.co</a> <span  className="subtext">Creative Director</span>
							</p>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</section>


	<section id="fh5co-services" data-section="services">
		<div  className="container">
			<div  className="row">
				<div  className="col-md-12 section-heading text-left">
					<h2  className=" left-border to-animate">Services</h2>
					<div  className="row">
						<div  className="col-md-8 subtext to-animate">
							<h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</h3>
						</div>
					</div>
				</div>
			</div>
			<div  className="row">
				<div  className="col-md-6 col-sm-6 fh5co-service to-animate">
					<i  className="icon to-animate-2 icon-anchor"></i>
					<h3>Brand &amp; Strategy</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean</p>
				</div>
				<div  className="col-md-6 col-sm-6 fh5co-service to-animate">
					<i  className="icon to-animate-2 icon-layers2"></i>
					<h3>Web &amp; Interface</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean</p>
				</div>

				<div  className="clearfix visible-sm-block"></div>

				<div  className="col-md-6 col-sm-6 fh5co-service to-animate">
					<i  className="icon to-animate-2 icon-video2"></i>
					<h3>Photo &amp; Video</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean</p>
				</div>
				<div  className="col-md-6 col-sm-6 fh5co-service to-animate">
					<i  className="icon to-animate-2 icon-monitor"></i>
					<h3>CMS &amp; eCommerce</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean</p>
				</div>
				
			</div>
		</div>
	</section>
	
	<section id="fh5co-about" data-section="about">
		<div  className="container">
			<div  className="row">
				<div  className="col-md-12 section-heading text-center">
					<h2  className="to-animate">About</h2>
					<div  className="row">
						<div  className="col-md-8 col-md-offset-2 subtext to-animate">
							<h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</h3>
						</div>
					</div>
				</div>
			</div>
			<div  className="row">
				<div  className="col-md-4">
					<div  className="fh5co-person text-center to-animate">
						<figure><img src="images/person3.jpg" alt="Image" /></figure>
						<h3>Jean Smith</h3>
						<span  className="fh5co-position">Web Designer</span>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
						<ul  className="social social-circle">
							<li><a href="#"><i  className="icon-twitter"></i></a></li>
							<li><a href="#"><i  className="icon-facebook"></i></a></li>
							<li><a href="#"><i  className="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>
				<div  className="col-md-4">
					<div  className="fh5co-person text-center to-animate">
						<figure><img src="images/person3.jpg" alt="Image"/></figure>
						<h3>Rob Smith</h3>
						<span  className="fh5co-position">Web Developer</span>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
						<ul  className="social social-circle">
							<li><a href="#"><i  className="icon-twitter"></i></a></li>
							<li><a href="#"><i  className="icon-facebook"></i></a></li>
							<li><a href="#"><i  className="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
				<div  className="col-md-4">
					<div  className="fh5co-person text-center to-animate">
						<figure><img src="images/person3.jpg" alt="Image"/></figure>
						<h3>Larry Ben</h3>
						<span  className="fh5co-position">Web Designer</span>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts</p>
						<ul  className="social social-circle">
							<li><a href="#"><i  className="icon-twitter"></i></a></li>
							<li><a href="#"><i  className="icon-facebook"></i></a></li>
							<li><a href="#"><i  className="icon-dribbble"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section id="fh5co-counters" style={{ backgroundImage: 'url(images/full_image_1.jpg)' }} data-stellar-background-ratio="0.5">
		<div  className="fh5co-overlay"></div>
		<div  className="container">
			<div  className="row">
				<div  className="col-md-12 section-heading text-center to-animate">
					<h2>Fun Facts</h2>
				</div>
			</div>
			<div  className="row">
				<div  className="col-md-3 col-sm-6 col-xs-12">
					<div  className="fh5co-counter to-animate">
						<i  className="fh5co-counter-icon icon-briefcase to-animate-2"></i>
						<span  className="fh5co-counter-number js-counter" data-from="0" data-to="89" data-speed="5000" data-refresh-interval="50">89</span>
						<span  className="fh5co-counter-label">Finished projects</span>
					</div>
				</div>
				<div  className="col-md-3 col-sm-6 col-xs-12">
					<div  className="fh5co-counter to-animate">
						<i  className="fh5co-counter-icon icon-code to-animate-2"></i>
						<span  className="fh5co-counter-number js-counter" data-from="0" data-to="2343409" data-speed="5000" data-refresh-interval="50">2343409</span>
						<span  className="fh5co-counter-label">Line of codes</span>
					</div>
				</div>
				<div  className="col-md-3 col-sm-6 col-xs-12">
					<div  className="fh5co-counter to-animate">
						<i  className="fh5co-counter-icon icon-cup to-animate-2"></i>
						<span  className="fh5co-counter-number js-counter" data-from="0" data-to="1302" data-speed="5000" data-refresh-interval="50">1302</span>
						<span  className="fh5co-counter-label">Cup of coffees</span>
					</div>
				</div>
				<div  className="col-md-3 col-sm-6 col-xs-12">
					<div  className="fh5co-counter to-animate">
						<i  className="fh5co-counter-icon icon-people to-animate-2"></i>
						<span  className="fh5co-counter-number js-counter" data-from="0" data-to="52" data-speed="5000" data-refresh-interval="50">52</span>
						<span  className="fh5co-counter-label">Happy clients</span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="fh5co-contact" data-section="contact">
		<div  className="container">
			<div  className="row">
				<div  className="col-md-12 section-heading text-center">
					<h2  className="to-animate">Get In Touch</h2>
					<div  className="row">
						<div  className="col-md-8 col-md-offset-2 subtext to-animate">
							<h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</h3>
						</div>
					</div>
				</div>
			</div>
			<div  className="row row-bottom-padded-md">
				<div  className="col-md-6 to-animate">
					<h3>Contact Info</h3>
					<ul  className="fh5co-contact-info">
						<li  className="fh5co-contact-address ">
							<i  className="icon-home"></i>
							5555 Love Paradise 56 New Clity 5655, <br/>Excel Tower United Kingdom
						</li>
						<li><i  className="icon-phone"></i> (123) 465-6789</li>
						<li><i  className="icon-envelope"></i>info@freehtml5.co</li>
						<li><i  className="icon-globe"></i> <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a></li>
					</ul>
				</div>

				<div  className="col-md-6 to-animate">
					<h3>Contact Form</h3>
					<div  className="form-group ">
						<label htmlFor="name"  className="sr-only">Name</label>
						<input id="name"  className="form-control" placeholder="Name" type="text"/>
					</div>
					<div  className="form-group ">
						<label htmlFor="email"  className="sr-only">Email</label>
						<input id="email"  className="form-control" placeholder="Email" type="email"/>
					</div>
					<div  className="form-group ">
						<label htmlFor="phone"  className="sr-only">Phone</label>
						<input id="phone"  className="form-control" placeholder="Phone" type="text"/>
					</div>
					<div  className="form-group ">
						<label htmlFor="message"  className="sr-only">Message</label>
						<textarea name="" id="message" cols="30" rows="5"  className="form-control" placeholder="Message"></textarea>
					</div>
					<div  className="form-group ">
						<input  className="btn btn-primary btn-lg" value="Send Message" type="submit" />
					</div>
					</div>
				</div>

			</div>
		
		<div id="map"  className="to-animate"></div>
	</section>

						</div>

	<footer id="footer" role="contentinfo">
		<a href="#"  className="gotop js-gotop"><i  className="icon-arrow-up2"></i></a>
		<div  className="container">
			<div  className="">
				<div  className="col-md-12 text-center">
					<p>&copy; Elate Free HTML5. All Rights Reserved. <br/>Created by <a href="http://freehtml5.co/" target="_blank">FREEHTML5.co</a> Images: <a href="http://pexels.com/" target="_blank">Pexels</a>, <a href="http://plmd.me/" target="_blank">plmd.me</a></p>
					
				</div>
			</div>
			<div  className="row">
				<div  className="col-md-12 text-center">
					<ul  className="social social-circle">
						<li><a href="#"><i  className="icon-twitter"></i></a></li>
						<li><a href="#"><i  className="icon-facebook"></i></a></li>
						<li><a href="#"><i  className="icon-youtube"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>      
  </div>
  
);
}
}

export default HomePage;



