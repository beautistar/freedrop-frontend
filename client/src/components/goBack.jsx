import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import { hashHistory } from 'react-router';
import TiArrowLeftThick from 'react-icons/lib/ti/arrow-left-thick';
import RaisedButton from 'material-ui/RaisedButton';

class Goback extends React.Component {	

  constructor(props, context) {
    super(props, context);
	 this.state={showBack:location.hash.split('?')[0]!=="#/"};
        this.hook = hashHistory.listenBefore(loc=>
            this.setState({showBack:loc.pathname!=="/"})
        );
  }
  
   componentWillUnmount(){
        this.hook(); //unlisten
    }
  
   render() {
    return (
	
		<RaisedButton label="Go Back" onClick={()=>hashHistory.goBack()} primary/> 
	
  
   )}
  }

export default Goback;
