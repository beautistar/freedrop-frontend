import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Card, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const SignUpForm = ({
  onSubmit,
  onChange,
  errors,
  user,
}) => (
 
    <form action="/" onSubmit={onSubmit}>
      <h2 className="card-heading" style={{color:"white"}}>Sign Up</h2>

      {errors.summary && <p className="error-message">{errors.summary}</p>}

      <div className="field-line">
        <TextField
          floatingLabelText="Name"
          name="name"
          errorText={errors.name}
          onChange={onChange}
          value={user.name}
		   floatingLabelStyle={{color: 'white' }}
		   inputStyle={{color: 'white' }}
        />
      </div>

      <div className="field-line">
        <TextField
          floatingLabelText="Email"
          name="email"
          errorText={errors.email}
          onChange={onChange}
          value={user.email}
		   floatingLabelStyle={{color: 'white' }}
		   inputStyle={{color: 'white' }}
        />
      </div>

      <div className="field-line">
        <TextField
          floatingLabelText="Password"
          type="password"
          name="password"
          onChange={onChange}
          errorText={errors.password}
          value={user.password}
		   floatingLabelStyle={{color: 'white' }}
		   inputStyle={{color: 'white' }}
        />
      </div>
	
      <div className="button-line">
        <RaisedButton type="submit" label="Create New Account" primary />
      </div>

      
    </form>

);

SignUpForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

export default SignUpForm;

