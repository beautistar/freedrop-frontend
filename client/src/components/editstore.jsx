import React, { PropTypes } from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Modal from 'react-modal';
import Validation from 'react-validation';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
//import { FormErrors } from './FormErrors'import './Form.css';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Goback from './goBack.jsx';
import GooglePicker from 'react-google-picker';
import { Link } from 'react-router';

import Header from '../components/header.jsx';

import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import Paper from 'material-ui/Paper';

const paperstyle = {
 
  margin: 20,
  textAlign: 'center',
  //display: 'inline-block',
};
  const myStyles = {
    root: { position: 'absolute' },
    input: { width: '100%', color:'#03A9F4' },
    autocompleteContainer: { backgroundColor: 'green' },
    autocompleteItem: { color: 'black' },
    autocompleteItemActive: { color: '#1332c7' }
  }
const styles = {
 underlineStyle: {
    borderColor: 'rgb(29, 63, 125)',
  },
  floatingLabelFocusStyle: {
    color: 'rgb(29, 63, 125)',
  },
}


class editstorePage extends React.Component {
	

  constructor(props) {
    super(props);

    this.state = {
       products : [{
      id: 1,
      name: "Product1",
      price: 120
  }, {
      id: 2,
      name: "Product2",
      price: 80
  }],
        errors: {},
      user: {
        email: '',
        name: '',
        password: ''
      },
	  address: '',
      geocodeResults: null,
      loading: false,
	  file: '',imagePreviewUrl: ''
	  
 
    };
	this.handleSelect = this.handleSelect.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.renderGeocodeFailure = this.renderGeocodeFailure.bind(this)
    this.renderGeocodeSuccess = this.renderGeocodeSuccess.bind(this)


  }//constructor closed
  
  
  _handleSubmit(e) {
    e.preventDefault();
    // TODO: do something with -> this.state.file
    console.log('handle uploading-', this.state.file);
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file)
  }
  
  
  handleSelect(address) {
    this.setState({
      address,
      loading: true
    })

    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then(({ lat, lng }) => {
        console.log('Success Yay', { lat, lng })
        this.setState({
          geocodeResults: this.renderGeocodeSuccess(lat, lng),
          loading: false
        })
      })
      .catch((error) => {
        console.log('Oh no!', error)
        this.setState({
          geocodeResults: this.renderGeocodeFailure(error),
          loading: false
        })
      })
  }

  handleChange(address) {
    this.setState({
      address,
      geocodeResults: null
    })
  }

  renderGeocodeFailure(err) {
    return (
      <div className="alert alert-danger" role="alert">
        <strong>Error!</strong> {err}
      </div>
    )
  }

  renderGeocodeSuccess(lat, lng) {
    return (
      <div className="alert alert-success" role="alert">
        <strong>Success!</strong> Geocoder found latitude and longitude: <strong>{lat}, {lng}</strong>
      </div>
    )
  }
  
  

  render() {
	  let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }
	  
	      const cssClasses = {
			root: 'form-group',
			input: 'my-input-id',
			autocompleteContainer: 'Demo__autocomplete-container',	
			
    }

    const AutocompleteItem = ({ formattedSuggestion }) => (
      <div className="Demo__suggestion-item">
        <i className='fa fa-map-marker Demo__suggestion-icon'/>
        <strong>{formattedSuggestion.mainText}</strong>{' '}
        <small className="text-muted">{formattedSuggestion.secondaryText}</small>
      </div>)

    const inputProps = {
      type: "text",
      value: this.state.address,
      onChange: this.handleChange,
      onBlur: () => { console.log('Blur event!'); },
      onFocus: () => { console.log('Focused!'); },
      //autoFocus: true,
      placeholder: "Location",
      name: 'Demo__input',
      id: "my-input-id",
	  
    }
	  
    return (
	<div id="full">
	<Header className="headerstyleShop" style={{marginTop:'0px'}}>

	  </Header>
				<div className="shopImg">
				<div className="fadeImage">
						<div className="container" style={{clear: 'both',paddingTop:'30px',wordWrap:'break-word'}}>
						<div className="card-heading1">
							<h1 className="white font40">Make easy money renting your storage space</h1>
							<h4 className="white font22">Store Luggage for Tourists </h4>
						</div>
						</div>
				  </div>
				</div>
				<div className="container-fluid greyBack">
				 <h3 className="textCentre font29 fontThemeColor margin_top30"><b>Edit Store</b></h3>
		
		<div className="container">
		<div className="row margin_top30" style={{clear: 'both'}}>
			<div className="col-sm-3 col-md-3">
			</div>
		
		<div className="col-sm-6 col-md-6">
	   
		  <Paper style={paperstyle} zDepth={5}>
				<div className="margin12">
					<div className="row">
						  <div className="col-sm-6 col-md-6 button-line">
								
							</div> 
							 <div className="col-sm-6 col-md-6 button-line">
								<Goback />
							 </div>
					</div>
				   <form action="/">
				  
						<div className="field-line">
						<TextField
						  floatingLabelText="Name"
						  name="name"
						  fullWidth={true}
						  underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
						/>
					  </div>
					  
						<div className="field-line">
						<TextField
						  floatingLabelText="Email Address"
						  name="name"
						  fullWidth={true}
							underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
						/>
					  </div>
					  
					  <div className="field-line">
						<TextField
						  floatingLabelText="Address"
						  name="password"
						  fullWidth={true}
						  underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
						/>
					  </div>
					   <div className="field-line">
						<TextField
						  floatingLabelText="City"
						  name="password"
						  fullWidth={true}
						  underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
						/>
					  </div>
					   <div className="field-line">
						<TextField
						  floatingLabelText="Bussiness Name"
						  name="password"
						  fullWidth={true}
						  underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
						/>
					  </div>
					   <div className="field-line">
						<TextField
						  floatingLabelText="Opening Hours"
						  name="password"
						  fullWidth={true}
						  underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
						/>
					  </div>
					
					  
					 <div className="field-line">
						<TextField
						  floatingLabelText="Payment Account"
						  name="password"
						  fullWidth={true}
						  underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
						/>
					  </div>
					  
					  
					   <div className="field-line">
						<TextField
						  floatingLabelText="Photo Url"
						  name="name"
						  fullWidth={true}
						  underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
						/>
					  </div>
					  
						 <div className="field-line">
						<TextField
						  floatingLabelText="Max number for storing bag and suitcase"
						  name="name"
						  fullWidth={true}
						  underlineFocusStyle={styles.underlineStyle}
							 floatingLabelFocusStyle={styles.floatingLabelFocusStyle}/>
					  </div>
					  
						 <div className="">
					
						 <PlacesAutocomplete
							onSelect={this.handleSelect}
							autocompleteItem={AutocompleteItem}
							onEnterKeyDown={this.handleSelect}
							classNames={cssClasses}
							inputProps={inputProps}
							styles={myStyles}
						  />
						  {this.state.loading ? <div><i className="fa fa-spinner fa-pulse fa-3x fa-fw Demo__spinner" /></div> : null}
						  {!this.state.loading && this.state.geocodeResults ?
							<div className='geocoding-results'>{this.state.geocodeResults}</div> :
						  null}
					  </div>
					
						  <div className="button-line">
								<RaisedButton disabled={this.state.submitDisabled} type="button" label="Save" primary />
							</div> 
						

					</form>
			</div>
		
			</Paper>
	  </div>
			
			<div className="col-sm-3 col-md-3">
			</div>
			
		</div>
		</div>
		</div>
</div>
	);
  }

}

export default editstorePage;
