import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';

let LogoutView;

LogoutView = class extends React.Component {
	
	 constructor(props, context) {
    super(props, context);
  }
	componentWillMount () {
		
        Auth.deauthenticateUser();
        /*this.context.router.push({
		  pathname: '/' } 
		);*/
		
		//this.context.router.replace('/');
    
	
	}
	
	
	componentDidMount() {
		
        //Auth.deauthenticateUser();
        this.context.router.push({
		  pathname: '/' } 
		);
		window.location.reload();
		
		//this.context.router.replace('/');
    
	
	}
    render () {
        return null;
    }

};

LogoutView.contextTypes = {
  router: PropTypes.object.isRequired
};

export default LogoutView;