import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Card, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Icon  from 'react-icon';
import SocialPerson from 'react-material-icons/icons/social/person';
import ActionLock from 'react-material-icons/icons/action/lock.js';
import Facebook from '../containers/facebook.jsx';
import Google from '../containers/google.jsx';



const LoginForm = ({
  onSubmit,
  onChange,
  errors,
  successMessage,
  user
}) => (
  
    <form action="/dashboard" onSubmit={onSubmit}>
      <h2 className="card-heading" style={{color:'white'}}>Login</h2>

      {successMessage && <p className="success-message">{successMessage}</p>}
      {errors.summary && <p className="error-message">{errors.summary}</p>}

      <div className="field-line">
	  <SocialPerson color={'white'} />
        <TextField
          floatingLabelText="Email"
          name="email"
          errorText={errors.email}
          onChange={onChange}
          value={user.email}
		  className="TextField"
		  floatingLabelStyle={{color: 'white' }}
		  inputStyle={{color: 'white' }}
		   
        />
      </div>

      <div className="field-line">
	  <ActionLock color={'white'} />
        <TextField
          floatingLabelText="Password"
          type="password"
          name="password"
          onChange={onChange}
          errorText={errors.password}
          value={user.password}
		  floatingLabelStyle={{color: 'white' }}
		  inputStyle={{color: 'white' }}
		  
        />
      </div>

      <div className="button-line">
        <RaisedButton type="submit" label="Log in" primary />
      </div>

      <CardText className="field-line">
	   <div  className="row">
				<div  className="col-md-12 text-center">
					<ul  className="social social-circle">
						{/*<li><a href="#"><i  className="icon-twitter"></i></a></li>
						<li><a href="#"><i  className="icon-facebook"></i></a></li>
						<li><a href="#"><i  className="icon-youtube"></i></a></li>
						<li><a href="#"><i  className="icon-google"></i></a></li>*/}
						<li style={{margin:'12px'}}><Facebook /></li>
						<li style={{margin:'12px'}}><Google /></li>
					</ul>
				</div>
			</div>
	   

	  </CardText>
    </form>
  
);

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  successMessage: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired
};

export default LoginForm;
