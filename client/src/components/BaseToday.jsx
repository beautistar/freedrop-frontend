import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import Auth from '../modules/Auth';
 const headerstyle= {
				backgroundColor: '#52d3aa',
				marginTop: '0',
				backgroundImage: '-webkit-gradient(linear, 0% 0%, 100% 100%, color-stop(0, #3f95ea), color-stop(1, #52d3aa))',
				backgroundImage: '-webkit-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)',
				backgroundImage: 'repeating-linear-gradient(to bottom right, #3f95ea 0%, #52d3aa 100%)',
				backgroundImage: '-ms-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)'
			}
const tagPath={} 
const external = "external"
		
//const Base = () => (
//class Base extends React.Component {
const Base = ({ children }) => (	
	
	/*serch_page(){
		
			window.location.href="http://"+location.host+'/#/search';

			location.reload()
	}*/
	


  <div>
	  
    <div className="top-bar">
      {/*<div className="top-bar-left">
        <IndexLink to="/">React App</IndexLink>
      </div>*/}
	   
	  <header role="banner" id="fh5co-header" style={"http://"+location.host+'/#/'!= location.href?headerstyle:tagPath }>
	 
                            <div className="container">	                       
                        
                                <nav className="navbar navbar-default">
                                    <div className="navbar-header">
                                
                                        <a href="#" className="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                                    <Link to="/" className="navbar-brand" >FreeDrop</Link> 
                                    </div>
                                    <div id="navbar" className="navbar-collapse collapse">
                                    <ul className="nav navbar-nav navbar-right">
									 { Auth.isUserAuthenticated() ?
									 <li className="dropdown"><a href="#" data-nav-section="home"><span>Booking</span></a>
                                        <ul className="dropdown-content"  style={"http://"+location.host+'/#/'!= location.href?headerstyle:tagPath }>
                                                <a href="#" ># of Bag, # of suitcase</a>
                                              <a href="#" >Time slot for using the service</a>
                                                <a href="#" >Location (Store)</a>
                                                <a href="#" >item</a>
                                             </ul>
                                        </li>
									 
									  :''
									 }
									 {/*{ location.href == "http://"+location.host+'/#/' ?
<li><Link to="#"data-nav-section="work"><span>Promotion Code</span></Link></li>:''}
										  
										  { location.href == "http://"+location.host+'/#/' ?
										  <li><Link to="/hostsignup" className="external"><span>Become a Host</span></Link></li>:''}*/}
										
										{ Auth.isUserAuthenticated() ?
											<li className="dropdown"><a href="#" className="dropbtn" data-nav-section="home"><span>Calendar</span></a>
                                          <ul className="dropdown-content"  style={"http://"+location.host+'/#/'!= location.href?headerstyle:tagPath }>
                                                <Link to="/booking" >Booking</Link>
                                                <Link to="/history" >History</Link>                                                
                                             </ul>
                                        </li>
										
										 :''
                                        }
	
												
                                        
	 <li> <Link  className="external" onClick={this.serch_page} style={{cursor:'pointer'}}><span>Search</span></Link> </li>
	
	 { Auth.isUserAuthenticated() ?  <li className="active dropdown"><a href="#" className="dropbtn" data-nav-section="home"><span>Payment Details</span></a>
                                         <ul className="dropdown-content"  style={"http://"+location.host+'/#/'!= location.href?headerstyle:tagPath }>
                                                <Link to="/stripe" >Stripe</Link>
                                                <Link to="/paypal" >Paypal</Link>
                                                <Link to="/credit" >Credit Card </Link>
                                                
                                                
                                             </ul>
                                        </li>:''
        
     }
	 
	 
	  { Auth.isUserAuthenticated()  ? <li><Link to="/profile"><span>Profile</span></Link>
                                        </li>:''
        
     }
	
	{ location.href == "http://"+location.host+'/#/store' ?
										  <li><Link to="/store/addstore"><span>Add Store</span></Link></li>:''}
										   
		{ location.href == "http://"+location.host+'/#/store' ?
										  <li><Link to="/store/editstore"><span>Edit Store</span></Link></li>:''}
	 
	 
                                        {Auth.isUserAuthenticated() ?     
         <li><Link to="/logout"><span>Log out</span></Link> </li>    
      : (   
			<li className="dropdown">
			<button className="dropbtn">Log in/Register</button>
			<ul className="dropdown-content"  style={"http://"+location.host+'/#/'!= location.href?headerstyle:tagPath }>
			<Link className="external" to="/parent">User</Link>
			<Link className="external" to="/store">Host</Link>
			</ul>
			</li>
		
	)}
	
	
	{/*{ Auth.isUserAuthenticated() ?'':
        <li> <Link  className="external" to="/signup"><span>Register</span></Link> </li>
	}*/}
	
	 
	 
	 
	 </ul>                                   
                                    </div>                                    
                                </nav>                                              
                        </div>
                    </header> 

    </div>

    { /* child component will be rendered here */ }
   {children}
	 

  </div>
  
);


    
//React.PropTypes.path1 ={location.pathname};

export default Base;
