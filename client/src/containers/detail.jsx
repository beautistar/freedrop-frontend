import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import Dashboard from '../components/Dashboard.jsx';
import Header from '../components/header.jsx';
import LoginPage from './LoginPage.jsx';
import { Link, IndexLink } from 'react-router';
import Spinner from 'react-spinner';
import Modal from 'react-modal';
import StripePage from './stripePage.jsx';
import PaypalPage from './paypalPage.jsx';
import MyAccount from '../components/myAccount.jsx';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import wrapper from './GoogleApiComponent.js'
import Map from '../components/mapComponent.jsx'
import Marker from '../components/markerComponent.jsx'
import GoogleMapsLoader from 'google-maps';

//GoogleMapsLoader.KEY = 'AIzaSyBQyFcuS_g7LNEOGk63wXcOYwG9lvRdVAA';


   const style = {
      width: '100%',
      height: '100px'
    }


class DetailPage extends React.Component {
	
  constructor(props) {
    super(props);
	this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
	//this.handleLocationError = this.handleLocationError.bind(this);
    /*this.state = {
      lat: '',
      lng: ''
    }*/

  }
  openModal() {
    this.setState({modalIsOpen: true});
  }
 
 afterOpenModal() {

  }
 closeModal() {
 
    this.setState({modalIsOpen: false});
  }
  componentDidMount() {
	  
    /*GoogleMapsLoader.load((google) => {
      var map = new google.maps.Map(document.getElementById('map'), {
       center: { lat: 37.759703, lng: -122.428093 },
       zoom: 10
      });
      // rest of the code with maps
    });*/
  }
 
	
  render() {
	  
	  
  
	 //const props = this.props;
    //const {google} = this.props;
	  
	    const style = {
      width: '100%',
      height: '450px'
    }
	const pos = {lat: 37.759703, lng: -122.428093}
    if (!this.props.loaded) {
		console.log("Console Loading...");
      return <div>Loading...</div>
    }
	  
	  const totalHeight = (screen.height-57) + "px";
	   
    return (
	 <div>
	  <Header className="headerstyleDetail" style={{marginTop:'0px'}}/>
	  <div className="bannerImg">
	  </div>
	  <div className="container">
			  <div className="col-xs-12 col-sm-12 col-md-12 margin_top30 margin_bottom20 listtxtalgn"><h2 style={{color: 'rgb(29, 63, 125)'}}><b>
		Euston -  St Pancras StashPoint - Left Luggage at Nisa (Open Late)</b></h2>
		 <p className="margin-bottompara">53-55 Chalton Street, Euston, London, NW1 1HY, England, United Kingdom</p>
		 <p className="margin-bottompara">Mon – Sat: 8am – 11pm, Sun: 9am – 11pm</p></div>
	  
			  <div className="row bottom-separate">
						<div className="col-sm-1 col-md-1"><span><i className="fa fa-suitcase iconDetail"></i></span>
						</div>
					  <div className="col-sm-5 col-md-5"> 
						<h3>Number of packages that can be stored</h3>
					  </div>
					  <div className="col-sm-6 col-md-6"> 
						<h4>Bag size: 5 Suitcase size: 3</h4>
					  </div>
					   
				</div>
				 
				  <div className="row bottom-separate">
						<div className="col-sm-1 col-md-1"><span><i className="fa fa-phone iconDetail"></i></span>
						</div>
					  <div className="col-sm-5 col-md-5"> 
						<h3>phone number</h3>
					  </div>
					  <div className="col-sm-6 col-md-6"> 
						<h4>45603-6455-1475</h4>
					  </div>
				</div>
				  <div className="row bottom-separate">
						<div className="col-sm-1 col-md-1"><span><i className="fa fa-clock-o iconDetail"></i></span>
						</div>
					  <div className="col-sm-5 col-md-5"> 
						<h3>business hours</h3>
					  </div>
					  <div className="col-sm-6 col-md-6"> 
						<h4>9: 00 AM - 7: 00 PM * Sat: 9: 00 AM - 9 : 00 PM * Sun: 9: 00 AM - 9: 00 PM</h4>
					  </div>
				</div>
				  <div className="row bottom-separate">
						<div className="col-sm-1 col-md-1"><span><i className="fa fa-bus iconDetail"></i></span>
						</div>
					  <div className="col-sm-5 col-md-5"> 
						<h3>The nearest station</h3>
					  </div>
					  <div className="col-sm-6 col-md-6"> 
						<h4>The nearest station</h4>
					  </div>
				</div>
				
				  <div className="row bottom-separate">
						<div className="col-sm-1 col-md-1"><span><i className="fa fa-home iconDetail"></i></span>
						</div>
					  <div className="col-sm-5 col-md-5"> 
						<h3>Facilities</h3>
					  </div>
					  <div className="col-sm-6 col-md-6"> 
						<div>
						<span><i className="fa fa-wifi iconDetail margin12"></i></span>WiFi
						</div>
						<div>
						<span><i className="fa fa-power-off iconDetail margin12"></i></span>Power Supply
						</div>
					
					  </div>
				</div>
				  <div className="row bottom-separate">
						<div className="col-sm-1 col-md-1"><span><i className="fa fa-external-link iconDetail"></i></span>
						</div>
					  <div className="col-sm-5 col-md-5"> 
						<h3>External Link</h3>
						
					  </div>
					  <div className="col-sm-6 col-md-6"> 
						<div>
						<span><i className="fa fa-facebook iconDetail margin12"></i></span>Facebook Page
						</div>
						<div>
						<span><i className="fa fa-twitter iconDetail margin12"></i></span>Twitter Page
						</div>
						<div>
						<span><i className="fa fa-instagram iconDetail margin12"></i></span>Instagram Account
						</div>
						
					  </div>
				</div>
				  <div className="row">
						<div className="col-sm-1 col-md-1"><span><i className="glyphicon glyphicon-map-marker iconDetail"></i></span>
						</div>
					  <div className="col-sm-5 col-md-5"> 
						<h3>Street address</h3>
					  </div>
					  <div className="col-sm-6 col-md-6"> 
						<h4>53-55 Chalton Street, Euston, London, NW1 1HY, England, United Kingdom</h4>
					  </div>
				</div>
				
			</div>{/*container closed*/}
				{/*<div className="mapImg bottom-separate">
  </div>*/}
				{/*****************************MAP *********************************************************/}
	<div className="map" style={style}>
		<Map google={this.props.google}

				center={{lat: 37.778519, lng: -122.405640}}
				centerAroundCurrentLocation={true}
				onClick={this.onMapClicked}
				containerStyle={{height:'100%'}}
				zoom={10}
				>
				<Marker
					onClick={this.onMarkerClick}
					title={'Marked by Renu'}
					name={'SOMA'}
					position={{lat: 37.778519, lng: -122.405640}} 
					/>
					<Marker />
				
				</Map>

	  </div> 
	  
	 
	{/*****************************MAP *********************************************************/}		
				
				<div className="container">
					<div className="col-xs-12 col-sm-12 col-md-12 margin_top30 margin_bottom20 listtxtalgn"><h2 style={{color: 'rgb(29, 63, 125)'}}><b>
		Euston -  St Pancras StashPoint - Left Luggage at Nisa (Open Late)</b></h2></div>
				
						 <div className="row  bottom-separate">
								<div className="col-sm-4 col-md-4">
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star-half-full iconStar"></i></span>
								<span><i className="fa fa-star-o iconStar"></i></span>
								<h3 className="margin12" style={{display: 'inline-block'}}>4.45</h3>
								<p>Independent Feedback based on 3 verified reviews.</p>
								</div>
							  <div className="col-sm-5 col-md-5"> 
							  </div>
							  <div className="col-sm-3 col-md-3"> 
							  </div>
						</div>
						
						 <div className="row bottom-separate">
								<div className="col-sm-6 col-md-6">
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star-half-full iconStar"></i></span>
								<span><i className="fa fa-star-o iconStar"></i></span>
								<h5 className="margin12" style={{display: 'inline-block'}}>Trusted Customer- 2 months ago</h5>
								<p>There was only one guy and he had to man the cash register and take the bags.</p>
								<span><i className="fa fa-thumbs-up iconLike"></i></span>
								<span className="margin12" style={{display: 'inline-block'}}><i className="	fa fa-share iconLike"></i></span>
								</div>
								
							  <div className="col-sm-6 col-md-6"> 
							  </div>
							 
						</div>
						
						 <div className="row bottom-separate">
								<div className="col-sm-6 col-md-6">
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star-half-full iconStar"></i></span>
								<span><i className="fa fa-star-o iconStar"></i></span>
								<h5 className="margin12" style={{display: 'inline-block'}}>Trusted Customer -2 months ago</h5>
								<p>There was only one guy and he had to man the cash register and take the bags.</p>
								<span><i className="fa fa-thumbs-up iconLike"></i></span>
								<span className="margin12" style={{display: 'inline-block'}}><i className="	fa fa-share iconLike"></i></span>
								</div>
							  <div className="col-sm-6 col-md-6"> 
							  </div>
							 
						</div>
						
							 <div className="row bottom-separate">
								<div className="col-sm-6 col-md-6">
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star iconStar"></i></span>
								<span><i className="fa fa-star-half-full iconStar"></i></span>
								<span><i className="fa fa-star-o iconStar"></i></span>
								<span><i className="fa fa-star-o iconStar"></i></span>
								<h5 className="margin12" style={{display: 'inline-block'}}>Trusted Customer -2 months ago</h5>
								<p>There was only one guy and he had to man the cash register and take the bags.</p>
								<span><i className="fa fa-thumbs-up iconLike"></i></span>
								<span className="margin12" style={{display: 'inline-block'}}><i className="	fa fa-share iconLike"></i></span>
								</div>
							  <div className="col-sm-6 col-md-6"> 
							  </div>
							 
						</div>
						
						
				
				</div>{/*container closed*/}
				
	</div>
 );
  }

}

//export default DetailPage;
export default wrapper({
  apiKey: 'AIzaSyBQyFcuS_g7LNEOGk63wXcOYwG9lvRdVAA'
})(DetailPage)
