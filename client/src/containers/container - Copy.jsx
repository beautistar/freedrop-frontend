
import React from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom' 
import {InfoWindow, GoogleApiWrapper} from 'google-maps-react'
import loadJS from 'loadJS'
import GoogleApi from '../GoogleApi'
import GoogleApiComponent from './GoogleApiComponent'
import ScriptCache from '../ScriptCache'
import Map from '../components/mapComponent.jsx'
import Marker from '../components/markerComponent.jsx'
import Contents from '../components/autocomplete.js'
export class Container extends React.Component {
  render() {
	  const props = this.props;
	  //const {position} = this.state;
	    const style = {
      width: '1000px',
      height: '100px'
    }
	const pos = {lat: 37.759703, lng: -122.428093}
	
	
    if (!this.props.loaded) {
		console.log("Loading...");
      return <div>Loading...</div>
    }
    return (
	<div className="row">
	<div className="col-sm-12 card-heading">
	 <div className="map" style={style}>Map will go here
	  <Map google={this.props.google}>
	  <Marker
          title={'Marked by Renu'}
          name={'SOMA'}
          position={{lat: 37.778519, lng: -122.405640}} />
        <Marker
		title={'Marked by Renu'}
          name={'Dolores park'}
          position={{lat: 22.7195687, lng: 75.85772580000003}} />
        <Marker />
		<Contents {...props} />
		
	  </Map>
	  </div> 
	  </div>
	  </div>
    )
  }
}

export default GoogleApiComponent({
  apiKey: 'AIzaSyBQyFcuS_g7LNEOGk63wXcOYwG9lvRdVAA'
})(Container)