import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import Dashboard from '../components/Dashboard.jsx';
import LoginPage from './LoginPage.jsx';
import { Link, IndexLink } from 'react-router';
import Spinner from 'react-spinner';
import Modal from 'react-modal';
import StripePage from './stripePage.jsx';
import PaypalPage from './paypalPage.jsx';
import MyAccount from '../components/myAccount.jsx';
import Header from '../components/header.jsx';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
var ReactScriptLoaderMixin = require('react-script-loader').ReactScriptLoaderMixin;
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
	marginTop			  : '0px',
	padding				   : '0px',
	backgroundColor:'rgba(0,0,0,.75)'
  },
  overlay: {
	  
	  //zIndex: '1000',
	  backgroundColor   : 'rgba(0, 0, 0, 0.75)'
	  
	  }
  
}; 
 const totalHeight = screen.height + "px";
class DashboardPage extends React.Component {
	  mixins: [ ReactScriptLoaderMixin ]
  
  /**
   * Class constructor.
   */
  constructor(props) {
    super(props);
	this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      secretData: '',
	 // userdata: ''
	  //data:
	  	
    };
  }
  openModal() {
  //openModal() {
    this.setState({modalIsOpen: true});
  }
 
 afterOpenModal() {
  //afterOpenModal() {
    // references are now sync'd and can be accessed. 
    //this.subtitle.style.color = '#f00';
  }
 closeModal() {
  //closeModal() {
    this.setState({modalIsOpen: false});
  }

  /**
   * This method will be executed after initial rendering.
   */
  componentDidMount() {
	   if (!this.props.loaded) {
		console.log("Console Loading...");
      return <Spinner />
    }
	
	  
    const xhr = new XMLHttpRequest();
    xhr.open('get', '/api/dashboard');
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    // set the authorization HTTP header
    xhr.setRequestHeader('Authorization', `bearer ${Auth.getToken()}`);
    xhr.responseType = 'json';
    xhr.addEventListener('load', () => {
      if (xhr.status === 200) {
        this.setState({
          secretData: xhr.response.message,
		  //userdata: xhr.response.data
        });
      }
    });
    xhr.send();
	//console.log(localStorage.getItem(user));
	
  }

  /**
   * Render the component.
   */
  render() {
	 
	  
    return (
	<div  className="dashboardStyle">
	<Header className="headerstyleLogin" style={{marginTop:'0px'}}/>
	 <div className="container">
	  <div style={{marginTop:'80px'}}>
	
		<div>
			   <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
		<button type="button" className="close" aria-label="Close" style={{fontSize:'50px'}} onClick={this.closeModal}>
			<span aria-hidden="true">&times;</span>
			</button>
		
		<div className="container paymentStyle">	 
		
			<div className="row">
			<div>
			  <ul className="nav nav-tabs">
				<li className="active margin12"><a  className="white" data-toggle="tab" href="#stripe">Stripe</a></li>
				<li className="margin12"><Link className="white" data-toggle="tab" to="#paypal">Paypal</Link></li>
			 </ul>
			 
			 </div>
			 
			 <div className="tab-content">
				<div id="stripe" className="tab-pane fade in active">
				  <StripePage />
				</div>
				
				<div id="paypal" className="tab-pane fade">
				  <PaypalPage />
				</div>
			 
			</div>
		</div>
		</div>
		
		</Modal>
		<div className="container dashboardBox">
				<div className="row"> 

					<div className="col-md-3" style={{marginTop:'30px'}} >

						  <ul className="nav nav-tabs">	
						  
						  <li className="active margin12"><Link data-toggle="tab" to="#menu1">My Account</Link></li>
							
							<li className="margin12"><a data-toggle="tab" href="#home">My Bookings</a></li>
							
							<li className="margin12" onClick={this.openModal}><a data-toggle="tab" href="#menu2">Payment Details</a></li>
							
						</ul>
					</div>
	
					<div className="col-md-9" style={{marginTop:'30px'}} >
		
							<div className="tab-content" >
									<div id="home" className="tab-pane fade">
									  <div className="row">	  
			
											  <ul className="nav nav-tabs">
												<li className="active margin12"><a data-toggle="tab" href="#upcoming">Upcoming Bookings</a></li>
												<li className="margin12"><Link data-toggle="tab" to="#previous">Previous Bookings</Link></li>
											 </ul>
										 
													<div className="tab-content">
										  
														<div id="upcoming" className="tab-pane fade in active">
														  <p>You dont have any current booking</p>
																
														</div>
											<div id="previous" className="tab-pane fade">
												 <p>You dont have any current booking</p> 
											</div>
										</div>
 
				</div>
									
									</div>
									<div id="menu1" className="tab-pane fade in active">
				 
										<div className="row">	  
			
											  <ul className="nav nav-tabs">
												<li className="active margin12"><a data-toggle="tab" href="#details">Edit Details</a></li>
												<li className="margin12"><Link data-toggle="tab" to="#pass">Edit Password</Link></li>
											 </ul>
										 
													<div className="tab-content">
										  
														<div id="details" className="tab-pane fade in active">
														  
																<div className="bs-example">
																<MyAccount />
																{/*<form>
																		<div className="form-group">
																		<label> First Name</label>
																		<input type="text" className="form-control" id="inputEmail" placeholder="First Name" required/>
																		</div>
																		<div className="form-group">
																		<label>Last name</label>
																		<input type="text" className="form-control" id="inputPassword" placeholder="Last name" required/>
																		</div>
																		<label>Gender</label>
																		<div className="radio">
																		<label><input type="radio"/>Male</label>
																		<label><input type="radio"/>Female</label>
																		</div>
																		<div className="form-group">
																		<label>Birthdate</label>
																		<input type="date" className="form-control" id="inputPassword" placeholder="Birthdate" required/>
																		</div>
																		<div className="form-group">
																		<label>Email Address</label>
																		<input type="email" className="form-control" id="inputPassword" placeholder="Email Address" required/>
																		</div>
																		<div className="checkbox">
																		<label><input type="checkbox"/> Remember me</label>
																		</div>
																		<p>We won't share your private email address with other FreeDrop users.</p>
																		<button type="submit" className="btn btn-primary">save</button>
  </form>*/}
																</div>
														</div>
												
												
											<div id="pass" className="tab-pane fade">
												 <div className="bs-example width50">
																<form onSubmit={this.onSubmit} >
														  
															<div className="field-line1">
															<TextField
															  floatingLabelText="Old Password"
															  name="email"
															  data-stripe='number'  
																inputStyle={{color: 'white' }}
																floatingLabelStyle={{color: 'white' }}
															/>
														  </div>
														  <div className="field-line1">
															<TextField
															  floatingLabelText="New Password"
															  name="email"
															  data-stripe='number'  
																inputStyle={{color: 'white' }}
																floatingLabelStyle={{color: 'white' }}
															/>
														  </div>
														  <div className="field-line1">
															<TextField
															  floatingLabelText="Confirm Password"
															  name="email"
															  data-stripe='number'  
																inputStyle={{color: 'white' }}
																floatingLabelStyle={{color: 'white' }}
															/>
														  </div>
														  <div className="button-line1">
															<RaisedButton disabled={this.state.submitDisabled} type="button" label="Save" primary />
														</div>
														   </form>
			 
											</div>
												</div>
										</div>	


 
				</div>
													
		  
		  </div>

		  </div>
		 
		  
	</div>
</div>

											
  </div>
	</div>
	  </div>
	</div>
	</div>
 );
  }

}

export default DashboardPage;
