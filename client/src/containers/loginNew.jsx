import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import SignUpForm from '../components/SignUpForm.jsx';
import Auth from '../modules/Auth';
import LoginForm from '../components/LoginForm.jsx';
import Loader from 'react-loader';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Dashboard from '../components/Dashboard.jsx';
import SignUpPage from './SignUpPage.jsx';
import LoginPage from './LoginPage.jsx';
import BackgroundImage from 'react-background-image-loader';
import Header from '../components/header.jsx';


const headerstyle={
	width:'100%',
	height:'100vh',
	
	backgroundImage: 'url(images/full_image_1.jpg)',
	backgroundSize: 'cover',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundColor: 'rgba(0,0,0,.6)',
   //opacity: 0.9;
	  
 
  //backgroundSize: 'cover',
  //overflow: 'hidden'
 
}

const localImage = 'images/full_image_2.jpg';
const source = 'images/full_image_2.jpg';

var Parent  = React.createClass({
  getInitialState:function(){
    return {signup:false,login:true}
  },
  switch:function(word){
    var signup,login;
    if(word == "signup"){signup = true;login = false;}
    else{login = true; signup = false;}
    return this.setState({login:login,signup:signup})
    
  },
  render:function(){
    
        var self = this;
        return (
		<div>
		<Header className="headerstyleLogin" style={{marginTop:'0px'}}/>
		
			<Card className="container loginStyle">
			<CardTitle
			  className="card-heading"
			  
			/>
		  
              <div className="container">
			  <div  className="boxstyle">
                      <div id="buttons">
					  
                        <p id="signupButton" onClick={self.switch.bind(null,"signup")} className={self.state.signup ? "aaa":"white"}>Sign Up</p>
						
                      <p id="loginButton" onClick={self.switch.bind(null,"login")} className={self.state.login ? "aaa":"white"}> Login</p>
                      </div>
              
                   { self.state.signup?<SignUpPage/> : null}
                   {self.state.login? <LoginPage /> : null}
				     { Auth.isUserAuthenticated()?<Dashboard />  :null}
        
     
				</div>	   
            
             </div>
			 </Card>
		
			</div>
        
        )
    
  }
})

export default Parent;