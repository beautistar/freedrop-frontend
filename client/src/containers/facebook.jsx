import React from 'react';
import { PropTypes } from 'react';
import mysql from 'mysql';
import FacebookLogin from 'react-facebook-login';
import TiSocialFacebook from 'react-icons/lib/ti/social-facebook';
import GoogleLogin from 'react-google-login';
import axios from 'axios';
import fetch from 'node-fetch';
import CircularJSON from 'circular-json';
import { Link } from 'react-router';
import { withRouter } from 'react-router';

import { Router, Route, IndexRoute, hashHistory, browserHistory, DefaultRoute } from 'react-router'

import request from 'superagent';
//import Auth1 from '../modules/Auth1.js';

  class Facebook extends React.Component {
	
	 constructor (props, context) {
    super(props, context);
	
	this.state = {
		token:''
	};
}
 componentDidMount(){
	 Facebook.contextTypes  = {
	router:  PropTypes.object.isRequired
};
  
 }
    responseGoogle(googleUser) {
		//var id_id = googleUser.getAuthResponse().id_id;
		//console.log({accessid: id_id});
		console.log("google user...."+JSON.stringify(googleUser));
		console.log("google user name ...."+JSON.stringify(googleUser.profileObj.name));
		console.log("google user email...."+JSON.stringify(googleUser.profileObj.email));
		
		console.log("google user imageUrl...."+JSON.stringify(googleUser.profileObj.imageUrl));
		console.log("google user googleId...."+JSON.stringify(googleUser.profileObj.googleId));
		
		 var request = new XMLHttpRequest();
		request.open('POST', 'http://localhost:3000/signup2', true);
		request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
		request.send(JSON.stringify(googleUser.profileObj));
}
      responseFacebook(response) {
		  console.log("facebook user..."+JSON.stringify(response));
		 console.log("user name..."+JSON.stringify(response.status));
		  var fbstatus = JSON.stringify(response.status);
	  
					/*request
				  .post('http://localhost:3001/signup1')
				  .send(response) // sends a JSON post body
				  
				  .set('accept', 'json')
				  .end((err, res) => {
					// Calling the end function will send the request
					console.log(res);
				  });*/
				  
			if(JSON.stringify(response.status)=='"unknown"'){
				console.log("pop up is closed by user");
			}	else{
				  
				  
				  
		 var xhr = new XMLHttpRequest();
		xhr.open('POST', 'http://localhost:3000/signup1', true);
		xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
		xhr.send(JSON.stringify(response));
		console.log("status is..."+xhr.status);
		
		xhr.onreadystatechange = function() {
		if (xhr.status == 200) {
				//console.log("status is..."+xhr.status);
					localStorage.setItem('token', xhr.response.id);
					//localStorage.setItem('user', xhr.response.name);
					/*this.context.router.push({
					pathname: '/dashboard' } 
					);*/
		
		hashHistory.push('/dashboard');
		//window.location.reload();
      }
	  }
			}		
		
		}

    render() {
		
      return (
	
		 
				<FacebookLogin
				tag="div"
				  appId="125318911430822"
				  autoLoad={false}
				  fields="name,email,picture"
				  scope="public_profile,user_friends,user_actions.books"
				  callback={this.responseFacebook}
				  icon={<TiSocialFacebook width="3em" height="3em"/>}
				  textButton=""
				  size="small"
			
				 cssClass="kep-login-facebook small hvr-bob"
				/>
			
		
	
	
      )
    }
  }
  
Facebook.contextTypes  = {
  router:  PropTypes.object.isRequired
};
  
  
export default Facebook;