import React from 'react';
import ReactDOM from 'react-dom';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Modal from 'react-modal';
import Validation from 'react-validation';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import RaisedButton from 'material-ui/RaisedButton';
import { hashHistory, Router } from 'react-router';
//import 'node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
//import { FormErrors } from './FormErrors'import './Form.css';



class BookingPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
       products : [{
      id: 1,
      name: "Product1",
      price: 120
  }, {
      id: 2,
      name: "Product2",
      price: 80
  }]
    };
  }
  
 render() {
    return (
	
	<Card className="container">
    <CardTitle
	  className="card-heading"
      title="Booking Page"
      subtitle="This is the booking page.."
	  
    />
	
	  <BootstrapTable striped hover>
      <TableHeaderColumn isKey dataField='id'>Location (Store)</TableHeaderColumn>
      <TableHeaderColumn dataField='name'># of Bag, # of suitcase</TableHeaderColumn>
      <TableHeaderColumn dataField='price'>Time slot for using the service</TableHeaderColumn>
  </BootstrapTable>
  
   <div className="button-line">
        <RaisedButton label="New Booking" onClick={() => hashHistory.push(`/search`)}       primary ></RaisedButton>
      </div>
	
	</Card>
	);
  }

}

export default BookingPage;
