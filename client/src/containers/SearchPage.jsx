import React from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Modal from 'react-modal';
import Validation from 'react-validation';
//import { FormErrors } from './FormErrors'import './Form.css';
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  },
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
};


class SearchPage extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props) {
    super(props);

    /*this.state = {
      secretData: ''
    };*/
	 this.state = {
		  modalIsOpen: false,
     email: '',
    password: '',
    formErrors: {email: '', password: ''},
    emailValid: false,
    passwordValid: false,
    formValid: false
  };
  this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
	
	
  }
  openModal() {
    this.setState({modalIsOpen: true});
  }
 
  afterOpenModal() {
    // references are now sync'd and can be accessed. 
    this.subtitle.style.color = '#f00';
  }
 
  closeModal() {
    this.setState({modalIsOpen: false});
  }
  
  /*handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value},
                  () => { this.validateField(name, value) });
  }

  validateField(fieldName,value){
    let fieldValidationErrors = this.state.formErrors;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;

    switch(fieldName) {
      case 'email':
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailValid ? '' : ' is invalid';
        break;
      case 'password':
        passwordValid = value.length >= 6;
        fieldValidationErrors.password = passwordValid ? '': ' is too short';
        break;
      default:
        break;
    }
    this.setState({formErrors: fieldValidationErrors,
                    emailValid: emailValid,
                    passwordValid: passwordValid
                  }, this.validateForm);
  }

  validateForm() {
    this.setState({formValid: this.state.emailValid && this.state.passwordValid});
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'has-error');
  }*/
	

  /**
   * This method will be executed after initial rendering.
   */
 
  render() {
    return (
	
	<Card className="container">
    <CardTitle
	  className="card-heading"
      title="Search Page"
      subtitle="This is the search page.."
	  
    />
	 <div>
        <button onClick={this.openModal}>Book</button>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
 
          <h2 ref={subtitle => this.subtitle = subtitle}>Your details</h2>
          <button onClick={this.closeModal}>close</button>
          <form className="demoForm">
       <h2>Sign up</h2>
       <div className="form-group">
         <label htmlFor="email">Email address</label>
         <input type="email" className="form-control"
           name="email" />
       </div>
	   <div className="form-group">
         <label htmlFor="mobile">Mobile</label>
         <input type="text" className="form-control"
           name="mobile" />
       </div>
       <div className="form-group">
         <label htmlFor="homecity">Home city</label>
         <input type="text" className="form-control"
           name="homecity" />
		   
       <div className="form-group">
         <label htmlFor="country">Country</label>
         <input type="text" className="form-control"
           name="country" />
       </div>
       </div>
       <button type="submit" className="btn btn-primary">
          Instant Book
       </button>
     </form>      </Modal>
      </div>
    );
  }
}
	</Card>
	);
  }

}

export default SearchPage;
