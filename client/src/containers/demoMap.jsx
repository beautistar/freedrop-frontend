import React from 'react';
import PropTypes from 'prop-types';
import shouldPureComponentUpdate from 'react-pure-render';

import GoogleMap from 'google-map-react';

class SimpleMap extends React.Component {
  static propTypes = {
    center: PropTypes.array,
    zoom: PropTypes.number,
    greatPlaceCoords: PropTypes.any,
	style: PropTypes.object
  };

  static defaultProps = {
    center: [59.938043, 30.337157],
    zoom: 9,
    greatPlaceCoords: {lat: 59.724465, lng: 30.080121},
	style: {height:'100%', width:'100%'}
  };

  shouldComponentUpdate = shouldPureComponentUpdate;

  constructor(props) {
    super(props);
  }

  render() {
    return (
       <GoogleMap
        // apiKey={YOUR_GOOGLE_MAP_API_KEY} // set if you need stats etc ...
        center={this.props.center}
        zoom={this.props.zoom}
        style={this.props.style}
		
		>
      
      </GoogleMap>
    );
  }
}

export default SimpleMap;