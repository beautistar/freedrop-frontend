import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import LoginForm from '../components/LoginForm.jsx';
import { Card, CardTitle, CardText } from 'material-ui/Card';
const headerstyle={
    backgroundColor: '#52d3aa',
    marginTop: '0',
    backgroundImage: '-webkit-gradient(linear, 0% 0%, 100% 100%, color-stop(0, #3f95ea), color-stop(1, #52d3aa))',
    backgroundImage: '-webkit-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)',
    backgroundImage: 'repeating-linear-gradient(to bottom right, #3f95ea 0%, #52d3aa 100%)',
    backgroundImage: '-ms-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)'
}

class Design extends React.Component {

 
  constructor(props, context) {
    super(props, context);

  
}

 
  
 

  render() {
    return (
		<Card className="container">
			<CardTitle
			  className="card-heading"
			  
			/>
	
			
	<div className="my-component">
	
	 <div className="container">
	<header>
		<h1>
			<a href="#">
				<img src="http://tfgms.com/sandbox/dailyui/logo-1.png" alt="Authentic Collection"/>
			</a>
		</h1>
	</header>
	<h1 className="text-center">Register</h1>
	<form className="registration-form">
		<label className="col-one-half">
			<span className="label-text">First Name</span>
			<input type="text" name="firstName"/>
		</label>
		<label className="col-one-half">
			<span className="label-text">Last Name</span>
			<input type="text" name="lastName"/>
		</label>
		<label>
			<span className="label-text">Email</span>
			<input type="text" name="email"/>
		</label>
		<label className="password">
			<span className="label-text">Password</span>
			<button className="toggle-visibility" title="toggle password visibility" tabIndex="-1">
				<span className="glyphicon glyphicon-eye-close"></span>
			</button>
			<input type="password" name="password"/>
		</label>
		<label className="checkbox">
			<input type="checkbox" name="newsletter"/>
			<span>Sign me up for the weekly newsletter.</span>
		</label>
		<div className="text-center">
			<button className="submit" name="register">Sign Me Up</button>
		</div>
	</form>
</div>

</div>
</Card>




    
    );
  }

}





export default Design;
