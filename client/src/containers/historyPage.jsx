import React from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Modal from 'react-modal';
import Validation from 'react-validation';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
//import { FormErrors } from './FormErrors'import './Form.css';

class HistoryPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
       products : [{
      id: 1,
      name: "Product1",
      price: 120
  }, {
      id: 2,
      name: "Product2",
      price: 80
  }]
    };
  }

  render() {
    return (
	
	<Card className="container">
    <CardTitle
	  className="card-heading"
      title="History Page"
      subtitle="This is the history page.."
	  
    />
	<BootstrapTable  striped hover>
      <TableHeaderColumn isKey dataField='id'>All Bookings</TableHeaderColumn>
      <TableHeaderColumn dataField='name'>Booking Date</TableHeaderColumn>
      <TableHeaderColumn dataField='price'>Transaction By</TableHeaderColumn>
  </BootstrapTable>
	
	
	</Card>
	);
  }

}

export default HistoryPage;
