
import PaypalExpressBtn from 'react-paypal-express-checkout';
import React from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import Modal from 'react-modal';
import Validation from 'react-validation';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

 class PaypalPage extends React.Component {
    render() {
        const client = {
            sandbox:    'access_token$sandbox$zfg6qxbpr5t9c6c2$b4df1e0184bd6fd45a2eeedb3c2dd75a',
            production: 'YOUR-PRODUCTION-APP-ID',
        }   
        return (
		<Card className="container" style={{backgroundColor:'transparent'}}>
		
		<div className="pay_stripe" style={{height:'350px'}}>
		
    <CardTitle
	  className="card-heading"
      title="Payment by Paypal"
      subtitle="Click the paypal button..."
	  titleColor="white"
	  subtitleColor="white"
	  
    />
            <PaypalExpressBtn client={client} currency={'USD'} total={1.00} />
			</div>
			</Card>
        );
    }
}
export default PaypalPage; 