//import React from 'react';
//import { Card, CardTitle, CardText } from 'material-ui/Card';
//import Modal from 'react-modal';
//import Validation from 'react-validation';
//import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import MonthPicker from './MonthPicker.jsx';
import Modal from 'react-modal';
var React = require('react');
import TimePicker from 'material-ui/TimePicker';
import DatePicker from 'material-ui/DatePicker';
import Header from '../components/header.jsx';
import Goback from '../components/goBack.jsx';

//import FlatButton from 'material-ui/FlatButton';
//import DateTimePicker from 'material-ui-datetimepicker';
var ReactScriptLoaderMixin = require('react-script-loader').ReactScriptLoaderMixin;

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  },
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
};




var Booking = React.createClass({
 

  getInitialState: function() {
    return {
      
      submitDisabled: false,
      modalIsOpen: false,
	  dateTime: null,
	  date: null,
    time: null,
    };
	 this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDate = this.handleDate.bind(this);
    this.handleTime = this.handleTime.bind(this);
	this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.disableRandomDates = this.disableRandomDates.bind(this);
	
  },
  
  
 
  /*setDate : function(dateTime) { 
  this.setState({ dateTime })
  },
  
  handleTime: function(event, time){
    this.setState({time: time})
  },
  
  handleDate: function(event, date){
    this.setState({date: date})
  },

  handleSubmit: function(event){
    let momentTime = moment(this.state.time);
    let momentDate = moment(this.state.date);
    let renderedDateTime = moment({
      year: momentDate.year(),
      month: momentDate.month(),
      day: momentDate.date(),
      hour: momentTime.hours(),
      minute: momentTime.minutes()
  });
     const newChore = {
      date_time: renderedDateTime,
    }
    this.props.actions.addEvent(newChore)
    this.setState({date: null, time: null});
  },*/
  
  
   showTimePicker: function() {
    this.refs.timepicker.openDialog();
  },

  openModal: function() {
  //openModal() {
    this.setState({modalIsOpen: true});
  },
 
 afterOpenModal: function() {
  //afterOpenModal() {
    // references are now sync'd and can be accessed. 
    //this.subtitle.style.color = '#f00';
  },
 closeModal: function() {
  //closeModal() {
    this.setState({modalIsOpen: false});
  },
  
  disableRandomDates: function() {
  return Math.random() > 0.7;
}, 
  
  render: function() {
	  const totalHeight = screen.height + "px";
	   /*const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        type='submit'
        onTouchTap={this.handleSubmit}
      />,
    ];*/

      return ( 	
	  <div className="bookstyle" style={{height:'100%'}} >
	  <Header className="headerstyleDetail" style={{marginTop:'0px'}}>
	  
	  </Header>
	  
	  
	  <div className="container">
	  <div className="pay_stripe1" style={{marginTop:'80px'}}>
	  
	  
  
	{/*<Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
	
	
	
	<div style={{ width: '230px'}}>
	
	<MonthPicker onChange={(date)=>{console.log(date);}} />
	</div>
  </Modal>*/}
	
			<div style={{ marginTop: '30px'}}>
			
			<form onSubmit={this.onSubmit}>
				<div><span>{ this.state.paymentError }</span></div><br />
				
			 <div className="field-line">
					<DatePicker
					  onChange={this.showTimePicker}
					  floatingLabelText="Drop off Date & Time"
							  inputStyle={{color: 'white'}}
							  floatingLabelStyle={{color: 'white' }}
					/>
					<TimePicker
					  ref="timepicker"
					  style={{ display: 'none' }}
					/>
			</div>
			
			
			
			 <div className="field-line">
				<DatePicker
				  onChange={this.showTimePicker}
				  floatingLabelText="Pick up Date & Time"
						  inputStyle={{color: 'white'}}
						  floatingLabelStyle={{color: 'white' }}
						  shouldDisableDate={this.disableRandomDates}
				/>
				<TimePicker
				  ref="timepicker"
				  style={{ display: 'none' }}
				/>
      </div>
			
			
			{/*<DatePicker onChange={this.handleDate} value ={this.state.date} hintText="Date to be completed by" />
          <TimePicker onChange={this.handleTime} value={this.state.time} hintText="Time to be completed by" />*/}
			
			
			
			 {/*<DateTimePicker onChange={this.setDate} />*/}
			
				
			  {/*<div className="field-line">
				<TextField
				  floatingLabelText="Drop off"
				  name="email"
				  data-stripe='exp-month' 
				inputStyle={{color: 'white'}}
				floatingLabelStyle={{color: 'white' }}				
				/>
				
			 </div>
			 {/*<RaisedButton disabled={this.state.submitDisabled} onClick={this.openModal} label="Choose time" primary />
			  <div className="field-line">
				<TextField
				  floatingLabelText="Pick up"
				  name="email"
				  data-stripe='exp-year' 
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
				{/*<RaisedButton disabled={this.state.submitDisabled} onClick={this.openModal} label="Choose time" primary />
			  </div>*/}
			  <div className="field-line">
				<TextField
				  floatingLabelText="Luggages"
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			   <div className="field-line">
				<TextField
				  floatingLabelText="Coupon code"
				  name="email"
				  data-stripe='cvc'  
				inputStyle={{color: 'white' }}	
				floatingLabelStyle={{color: 'white' }}				
				/>
			  </div>
			  
			  <div className="white margin46"><strong>Total:</strong></div>
			  <hr className="details-line"></hr>
			  <div className="white field-line">Personal Details:</div>
			  
			  	<div className="field-line">
				<TextField
				  floatingLabelText="Full Name"
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  <div className="field-line">
				<TextField
				  floatingLabelText="Home City"
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  <div className="field-line">
				<TextField
				  floatingLabelText="Country"
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  
			  
			  
			  <div className="row">
					  <div className="col-sm-6 col-md-6 button-line">
										<RaisedButton disabled={this.state.submitDisabled} type="button" label="Book" primary />
						</div> 
						 <div className="col-sm-6 col-md-6 button-line">
							<Goback />
						 </div>
				
			  </div>
			  
			  </form>
			  </div>
			</div>
	  
	  
	  </div>
	  </div>
	  
	  
	  );
    
  }
});


module.exports = Booking;
