import React, {PropTypes, Component} from 'react';
import shouldPureComponentUpdate from 'react-pure-render';
import GoogleMap from 'google-map-react';
//import Marker from 'google-map-react';
import Marker from './Marker.js';
import MyGreatPlace from './my_great_place.jsx';
import ReactDOM from 'react-dom';
import Icon from 'react-icon';
import RaisedButton from 'material-ui/RaisedButton';


export default class SimpleMapPage extends Component {
  static propTypes = {
    center: PropTypes.array,
    zoom: PropTypes.number,
    greatPlaceCoords: PropTypes.any,
	 placeholder: React.PropTypes.string,
    onPlacesChanged: React.PropTypes.func
  };
  shouldComponentUpdate = shouldPureComponentUpdate;

  constructor(props) {
    super(props);
	this.state={
		center: [ 43.4970938,  121.9943049],
		zoom: 10,
		greatPlaceCoords: { lat: 43.4970938, lng: 121.9943049 }
		
	  };
  }

  render() {
	 return (
	<div className="row">
		<div className='col-sm-6 card-heading'>
			<input ref="input" { ...this.props.onPlacesChanged} type="text"/>
			<div className="row marginleft12">
			<div className='col-sm-6 col-md-6 card-heading'>
			<img src={'images/h1.jpg'} alt="boohoo" className="img-responsive"/>
			<div className="floatLeft">
				
				<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
				
				<p className="margin5">
				30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
				</p>
				<p className="margin5">
				Open all hours! 24 hours a day, 7 days a week
				</p>
				<div className="row">
				<div className='col-sm-5'>
				<RaisedButton  className="button-default" type="submit" label="Details" />
				</div>
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Book" primary />
				</div>
				<div className='col-sm-1'>
				<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
				</div>
				<div className='col-sm-1'>
				</div>
				</div>
				
				
				
			</div>
			</div>
			<div className='col-sm-6 col-md-6 card-heading'>
			<img src={'images/h2.jpg'} alt="boohoo" className="img-responsive"/>
			<div className="floatLeft">
				
				<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
				
				<p className="margin5">
				30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
				</p>
				<p className="margin5">
				Open all hours! 24 hours a day, 7 days a week
				</p>
				<div className="row">
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Details" default />
				</div>
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Book" primary />
				</div>
				<div className='col-sm-1'>
				<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
				</div>
				<div className='col-sm-1'>
				
				</div>
				</div>
			</div>
			</div>
			<div className='col-sm-6 card-heading'>
			</div>
			</div>
			
		</div>
		<div className="col-sm-6 card-heading">
			<div className="map">
		   <GoogleMap
			// apiKey={YOUR_GOOGLE_MAP_API_KEY} // set if you need stats etc ...
			//apiKey='AIzaSyBQyFcuS_g7LNEOGk63wXcOYwG9lvRdVAA'
			//bootstrapURLKeys={{key: 'AIzaSyBQyFcuS_g7LNEOGk63wXcOYwG9lvRdVAA'}} 
			center={this.state.center}
			zoom={this.state.zoom}>
			<MyGreatPlace lat={59.955413} lng={30.337844} text={'China Yanji'} /* Kreyser Avrora */ />
			<MyGreatPlace {...this.state.greatPlaceCoords} text={'China Yanji'} /* road circle */ />
		  </GoogleMap>
		  </div>
		</div>
	  </div>
    );
  }
   onPlacesChanged = () => {
	 
   // if (this.props.onPlacesChanged) {
   //   this.props.onPlacesChanged(this.searchBox.getPlaces());
	  	 //console.log(JSON.stringify(this.searchBox.getPlaces()));
			 var loc=this.searchBox.getPlaces()[0];
			 console.log(loc.geometry.location.lat());
			 console.log(loc.geometry.location.lng());
			// console.log(loc.geometry.viewport.northeast.lng);
			
			// this.props.center=[loc.geometry.location.lat(),loc.geometry.location.lng()]
			this.setState({
				center: [loc.geometry.location.lat(),loc.geometry.location.lng()],
				greatPlaceCoords: [loc.geometry.location.lat(),loc.geometry.location.lng()],
				
			});
	  
   // }
  }
  componentDidMount() {
    var input = ReactDOM.findDOMNode(this.refs.input);
    this.searchBox = new google.maps.places.SearchBox(input);
    this.searchBox.addListener('places_changed', this.onPlacesChanged);
  }
  componentWillUnmount() {
    this.searchBox.removeListener('places_changed', this.onPlacesChanged);
  }
}