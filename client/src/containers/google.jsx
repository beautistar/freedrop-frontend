import React from 'react';
import { PropTypes } from 'react';
import mysql from 'mysql';

import TiSocialGooglePlus from 'react-icons/lib/ti/social-google-plus';
import GoogleLogin from 'react-google-login';
import axios from 'axios';
import fetch from 'node-fetch';
import CircularJSON from 'circular-json';
import { Link } from 'react-router';
import { withRouter } from 'react-router';


import { Router, Route, IndexRoute, hashHistory, browserHistory, DefaultRoute } from 'react-router'

import request from 'superagent';

let iconStyles = {
  fontSize: '48px'
};
  class Google extends React.Component {
	
	 constructor (props, context) {
    super(props, context);
	
	this.state = {
		token:''
	};
}
 componentDidMount(){
	 Google.contextTypes  = {
	router:  PropTypes.object.isRequired
};
  
 }
    responseGoogle(googleUser) {
		console.log("google user...."+JSON.stringify(googleUser));
		console.log("google user name ...."+JSON.stringify(googleUser.profileObj.name));
		console.log("google user email...."+JSON.stringify(googleUser.profileObj.email));
		
		console.log("google user imageUrl...."+JSON.stringify(googleUser.profileObj.imageUrl));
		console.log("google user googleId...."+JSON.stringify(googleUser.profileObj.googleId));
		
		 var xhr = new XMLHttpRequest();
		xhr.open('POST', 'http://localhost:3000/signup2', true);
		xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
		xhr.send(JSON.stringify(googleUser.profileObj));
		console.log("status is..."+xhr.status);
		
		xhr.onreadystatechange = function() {
		if (xhr.status == 200) {
				//console.log("status is..."+xhr.status);
					localStorage.setItem('token', xhr.response.id);
					//localStorage.setItem('user', xhr.response.name);
					/*this.context.router.push({
					pathname: '/dashboard' } 
					);*/
		
		hashHistory.push('/dashboard');
		//window.location.reload();
      }
	  }
	}
      

    render() {
		
      return (
				<GoogleLogin clientId="100792857870-gn36dlk4s4o5vbk845l58uqm3d5f2elb.apps.googleusercontent.com"
							tag="span"
							onSuccess={this.responseGoogle}
							onFailure={this.responseGoogle}
							style={{display: 'inline-block', background: 'rgb(209, 72, 54)', color: 'rgb(255, 255, 255)', cursor:'pointer'}}
							className="hvr-bob"
							>
							<TiSocialGooglePlus width="3.5em" height="3.5em" />
				</GoogleLogin>			
	
      )
    }
  }
  
Google.contextTypes  = {
  router:  PropTypes.object.isRequired
};
  
  
export default Google;