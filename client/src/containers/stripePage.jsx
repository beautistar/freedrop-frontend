//import React from 'react';
//import { Card, CardTitle, CardText } from 'material-ui/Card';
//import Modal from 'react-modal';
//import Validation from 'react-validation';
//import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import MonthPicker from './MonthPicker.jsx';
import DatePicker from 'material-ui/DatePicker';
import Modal from 'react-modal';
var React = require('react');
var ReactScriptLoaderMixin = require('react-script-loader').ReactScriptLoaderMixin;
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  },
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
};


var StripePage = React.createClass({
  mixins: [ ReactScriptLoaderMixin ],

  getInitialState: function() {
    return {
      stripeLoading: true,
      stripeLoadingError: false,
      submitDisabled: false,
      paymentError: null,
      paymentComplete: false,
      token: null,
	  modalIsOpen: false,
    };
	this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
	
  },

  getScriptURL: function() {
    return 'https://js.stripe.com/v2/';
  },

  onScriptLoaded: function() {
    if (!StripePage.getStripeToken) {
      // Put your publishable key here
      Stripe.setPublishableKey('pk_test_xxxx');

      this.setState({ stripeLoading: false, stripeLoadingError: false });
    }
  },

  onScriptError: function() {
    this.setState({ stripeLoading: false, stripeLoadingError: true });
  },

  onSubmit: function(event) {
    var self = this;
    event.preventDefault();
    this.setState({ submitDisabled: true, paymentError: null });
    // send form here
    Stripe.createToken(event.target, function(status, response) {
      if (response.error) {
        self.setState({ paymentError: response.error.message, submitDisabled: false });
      }
      else {
        self.setState({ paymentComplete: true, submitDisabled: false, token: response.id });
        // make request to your server here!
      }
    });
  },
  openModal: function() {
  //openModal() {
    this.setState({modalIsOpen: true});
  },
 
 afterOpenModal: function() {
  //afterOpenModal() {
    // references are now sync'd and can be accessed. 
    //this.subtitle.style.color = '#f00';
  },
 closeModal: function() {
  //closeModal() {
    this.setState({modalIsOpen: false});
  },
  
  render: function() {
	 
	  
	  
	  
    if (this.state.stripeLoading) {
      return <div>Loading</div>;
    }
    else if (this.state.stripeLoadingError) {
      return <div>Error</div>;
    }
    else if (this.state.paymentComplete) {
      return <div>Payment Complete!</div>;
    }
    else {
      return ( 	<Card className="container" style={{backgroundColor:'transparent'}}>
	  <div className="pay_stripe">
	  
    <CardTitle
	  className="card-heading1"
      title="Payment by Stripe"
      subtitle="Please enter your account details.."
	  titleColor="white"
	  subtitleColor="white"
	  
    />
	
	
	 
	
	   <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
	
	
	
	<div style={{ width: '230px'}}>
	
	<MonthPicker onChange={(date)=>{console.log(date);}} />
	</div>
	</Modal>
	
	
	
	
			<div>
			
			
			
			<form onSubmit={this.onSubmit} >
				<div><span>{ this.state.paymentError }</span></div><br />
				
				
				
					<div className="field-line">
				<TextField
				  floatingLabelText="credit card number"
				  name="email"
				  data-stripe='number'  
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  <div className="field-line">
				<DatePicker hintText="Portrait Dialog"
				floatingLabelText="expiration month"
				inputStyle={{color: 'white'}}
				floatingLabelStyle={{color: 'white' }}
				/>
			  </div>
			  
			   <div className="field-line">
				<DatePicker
				floatingLabelText="expiration year"
				inputStyle={{color: 'white'}}
				floatingLabelStyle={{color: 'white' }}
				openToYearSelection={true}
				/>
			  </div>
			  
			  
			  
			  {/*<TextField
				  floatingLabelText="expiration month"
				  name="email"
				  data-stripe='exp-month' 
				inputStyle={{color: 'white'}}
				floatingLabelStyle={{color: 'white' }}				
				/>
	<RaisedButton disabled={this.state.submitDisabled} onClick={this.openModal} label="Choose Month" primary />
			 
			  <div className="field-line">
				<TextField
				  floatingLabelText="expiration year"
				  name="email"
				  data-stripe='exp-year' 
					inputStyle={{color: 'white' }}
					floatingLabelStyle={{color: 'white' }}
				/>
				<RaisedButton disabled={this.state.submitDisabled} onClick={this.openModal} label="Choose Year" primary />
			  </div>*/}
			   <div className="field-line">
				<TextField
				  floatingLabelText="cvc"
				  name="email"
				  data-stripe='cvc'  
				inputStyle={{color: 'white' }}	
				floatingLabelStyle={{color: 'white' }}				
				/>
			  </div>
				<div className="button-line">
				<RaisedButton disabled={this.state.submitDisabled} type="submit" label="Purchase" primary />
			  </div>
			  </form>
			  
			  
			  
			  
			  </div>
			</div>
	  
	  </Card>
	  
	  );
    }
  }
});



module.exports = StripePage;
