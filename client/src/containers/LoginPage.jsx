import React, { PropTypes } from 'react';
import Auth from '../modules/Auth';
import LoginForm from '../components/LoginForm.jsx';
import Loader from 'react-loader';

const headerstyle={
    backgroundColor: '#52d3aa',
    marginTop: '0',
    backgroundImage: '-webkit-gradient(linear, 0% 0%, 100% 100%, color-stop(0, #3f95ea), color-stop(1, #52d3aa))',
    backgroundImage: '-webkit-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)',
    backgroundImage: 'repeating-linear-gradient(to bottom right, #3f95ea 0%, #52d3aa 100%)',
    backgroundImage: '-ms-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)'
}
const options = {
    lines: 13,
    length: 20,
    width: 10,
    radius: 30,
    scale: 1.00,
    corners: 1,
    color: '#000',
    opacity: 0,
    rotate: 0,
    direction: 1,
    speed: 1,
    trail: 60,
    fps: 20,
    zIndex: 2e9,
    top: '50%',
    left: '50%',
    shadow: false,
    hwaccel: false,
    position: 'absolute'
};

class LoginPage extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }// set the initial component state
    this.state = {
      errors: {},
      successMessage,
      user: {
        email: '',
        password: ''
      },
	  loaded: true
    };

    this.processForm = this.processForm.bind(this);
    this.changeUser = this.changeUser.bind(this);
  }

  /**
   * Process the form.
   *
   * @param {object} event - the JavaScript event object
   */
  processForm(event) {
    // prevent default action. in this case, action is the form submission event
    event.preventDefault();
	 this.setState({
		  loaded: false
        });
	

    // create a string for an HTTP body message
    const email = encodeURIComponent(this.state.user.email);
    const password = encodeURIComponent(this.state.user.password);
    const formData = `email=${email}&password=${password}`;

    // create an AJAX request
    const xhr = new XMLHttpRequest();
    xhr.open('post', '/auth/login');
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.responseType = 'json';
    xhr.addEventListener('load', () => {
      if (xhr.status === 200 && xhr.response.token!=false) {
        // success

        // change the component-container state
        this.setState({
          errors: {},
		  loaded: false
        });

        // save the token
        Auth.authenticateUser(xhr.response.token);
		localStorage.setItem('user', xhr.response.user);
	
		//console.log(localStorage.getItem(user));
		

        // change the current URL to /
        //this.context.router.replace('/dashboard');
		this.context.router.push({
		  pathname: '/dashboard'
		  /*query: { data: JSON.stringify(xhr.response.user)*/ } 
		);
		
		console.log("data+++++++"+xhr.response.message);
		console.log("data+++++++"+JSON.stringify(xhr.response.user));
      } else {
        // failure
			//alert(xhr.response.user);
        // change the component state
        const errors = xhr.response.errors ? xhr.response.errors : {};
        errors.summary = xhr.response.message;

        this.setState({
          errors
        });
      }
    });
    xhr.send(formData);
	console.log("Form data "+formData);
	
  }
  
  //console.log("data+++++++"+JSON.stringify(xhr.response.user));

  /**
   * Change the user object.
   *
   * @param {object} event - the JavaScript event object
   */
  changeUser(event) {
    const field = event.target.name;
    const user = this.state.user;
    user[field] = event.target.value;

    this.setState({
      user
    });
  }

  /**
   * Render the component.
   */
  render() {
    return (
	<div>
	
	 <Loader loaded={this.state.loaded} options={options} className="spinner" />
	
      <LoginForm
        onSubmit={this.processForm}
        onChange={this.changeUser}
        errors={this.state.errors}
        successMessage={this.state.successMessage}
        user={this.state.user}
      />
	   
	  
	</div>  
    );
  }

}

LoginPage.contextTypes = {
  router: PropTypes.object.isRequired
};

export default LoginPage;
