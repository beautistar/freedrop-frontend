import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Auth from '../modules/Auth';
import LoginPage from './LoginPage.jsx';
import SignUpPageHost from './SignUpPageHost.jsx';

import Header from '../components/header.jsx';
import { Card, CardTitle, CardText } from 'material-ui/Card';
const headerstyle={
	width:'100%',
	height:'100vh',
	backgroundImage: 'url(images/full_image_1.jpg)',
	backgroundSize: 'cover',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundColor: 'rgba(0,0,0,.6)'
	  
 
  //backgroundSize: 'cover',
  //overflow: 'hidden'
 
}

var StorePage  = React.createClass({
  getInitialState:function(){
    return {signup:false,login:true}
  },
  switch:function(word){
    var signup,login;
    if(word == "signup"){signup = true;login = false;}
    else{login = true; signup = false;}
    return this.setState({login:login,signup:signup})
    
  },
  render:function(){
    
        var self = this;
        return (
		<div>
		<Header className="headerstyleLogin" style={{marginTop:'0px'}}/>
		<Card className="container loginStyle" style={headerstyle}>
			<CardTitle
			  className="card-heading"
			  
			/>
		  
              <div>
			  <div  className="boxstyle">
                      <div id="buttons">
                        <p id="signupButton" onClick={self.switch.bind(null,"signup")} className={self.state.signup ? "aaa":"blue"}>Sign Up</p>
                      <p id="loginButton" onClick={self.switch.bind(null,"login")} className={self.state.login ? "aaa":"blue"}> Login</p>
                      </div>
              
                   { self.state.signup?<SignUpPageHost/> : null}
                   {self.state.login? <LoginPage /> : null}
					   
            
             </div>
             </div>
			 </Card>
			</div>
        
        )
    
    
  }
})



export default StorePage;
