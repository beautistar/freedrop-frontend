import React from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom' 
import {GoogleApiWrapper} from 'google-maps-react'
import GoogleApi from '../GoogleApi'
//import GoogleApiComponent from './GoogleApiComponent.js'
import wrapper from './GoogleApiComponent.js'
import ScriptCache from '../ScriptCache'
import Map from '../components/mapComponent.jsx'
import InfoWindow from '../components/InfoWindow.js'
import Marker from '../components/markerComponent.jsx'
import Header from '../components/header.jsx'
import { Router, Route, Link, IndexRoute, hashHistory, browserHistory, DefaultRoute } from 'react-router'
//import Contents from '../components/autocomplete.js'
import RaisedButton from 'material-ui/RaisedButton';
import Modal from 'react-modal';
import Booking from './booking.jsx';
import DetailPage from './detail.jsx';
import Iframe from 'react-iframe'
import Checkbox from 'material-ui/Checkbox';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import Visibility from 'material-ui/svg-icons/action/visibility';
import VisibilityOff from 'material-ui/svg-icons/action/visibility-off';
import invariant from 'invariant';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
	marginTop			  : '150px',
	padding				   : '0px',
	backgroundColor:'rgba(0,0,0,.75)'
  },
  overlay: {
	  
	  //zIndex: '1000',
	  backgroundColor   : 'rgba(0, 0, 0, 0.75)',
	 
	  
	  }
  
};
const styles = {
  block: {
    maxWidth: 250,
	
  },
  checkbox: {
    marginBottom: 16,

  },
};

var indents = [];
const divstyle={
	width:'100%',
	height:'100%',
	backgroundImage: 'url(images/img_15.jpg)',
	backgroundSize: 'cover',
	backgroundPosition: 'center',
	backgroundRepeat: 'no-repeat',
	backgroundColor: 'rgba(0,0,0,.6)',
	
	
}
const headerstyle= {
				backgroundColor: '#52d3aa',
				marginTop: '0',
				backgroundImage:'url(images/navbar.jpg)'
			}


const Contents = React.createClass({
  getInitialState() {
    return {
      place: null,
      position: null,
	 showingInfoWindow: false,
     activeMarker: {},
      selectedPlace: {},
    }
  },

  onMarkerClick: function(props, marker, e) {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
    });
  },

  onInfoWindowClose: function() {
    this.setState({
      showingInfoWindow: false,
      activeMarker: null
    })
  },

  onMapClicked: function(props) {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  },

  onSubmit: function(e) {
    e.preventDefault();
  },
  
  loop :function(){
	  for (var i = 0; i <11; i++) {
	  indents.push( <ul className="paddingLeft12" key={i}>
					<li>
					
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb hvr-float-shadow">
									<img src='images/c1.jpg' alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
						
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" onClick={() => {hashHistory.push('/detail'); window.location.reload();}} label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={() => hashHistory.push('/booking')} primary />
														</div>
														<div className='col-sm-1'>
														{/*<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>*/}
														 <Checkbox
														  checkedIcon={<ActionFavorite />}
														  uncheckedIcon={<ActionFavoriteBorder />}
															iconStyle={{color:'red'}}
														  style={styles.checkbox}
														  
														/>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
		 </ul>
		 );
	}
  },

  componentDidMount: function() {
	
    this.renderAutoComplete();
	console.log("componentDidMount of container");
	 //alert("componentDidMount of container");
  
	
  
  },

  componentDidUpdate(prevProps) {
    const {google, map} = this.props;
    if (map !== prevProps.map) {
      this.renderAutoComplete();
	  console.log("componentDidUpdate of container");
	  //alert("componentDidUpdate of container");
    }
	
	
  },

  renderAutoComplete: function() {
    const {google, map} = this.props;

    if (!google || !map) return;

    const aref = this.refs.autocomplete;
    const node = ReactDOM.findDOMNode(aref);
    var autocomplete = new google.maps.places.Autocomplete(node);
    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) {
        return;
      }

      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(7);
      }

      this.setState({
        place: place,
        position: place.geometry.location
      })
    })
  }, openModal: function() {
  //openModal() {
    this.setState({modalIsOpen: true});
  },
 
 afterOpenModal: function() {
  //afterOpenModal() {
    // references are now sync'd and can be accessed. 
    //this.subtitle.style.color = '#f00';
  },
 closeModal: function() {
  //closeModal() {
    this.setState({modalIsOpen: false});
  },
  render: function() {
    const props = this.props;
    const {position} = this.state;
	  this.loop();
	  const totalHeight = (screen.height-57) + "px";

    return (
	
      <div className="row">
	   <Modal
				  isOpen={this.state.modalIsOpen}
				  onAfterOpen={this.afterOpenModal}
				  onRequestClose={this.closeModal}
				  style={customStyles}
				  contentLabel="Example Modal"
				>
				<div style={divstyle}>
				<button type="button" className="close" aria-label="Close" style={{fontSize:'50px'}} onClick={this.closeModal}>
					<span aria-hidden="true">&times;</span>
					</button>
					<Booking />
					</div>
			</Modal>
				<div className="col-sm-6 col-md-5" style={{height:'100vh', overflowY: 'scroll', paddingTop: '30px',backgroundColor: 'rgba(0, 42, 56, 0.91)'}} >
					  <form onSubmit={this.onSubmit}>
						<input
						  ref='autocomplete'
						  type="text"
						  placeholder="Enter a location" />
						<RaisedButton type="submit" label="Search" primary />
					  </form>
					  <div>
						<div>Lat: {position && position.lat()}</div>
						<div>Lng: {position && position.lng()}</div>
					  </div>
					  
					  
					  
				 <div className="row card-heading-centres">
				 
				    {indents}
				 
				 {/*<ul className="paddingLeft12">
					<li>
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb">
									<img src={'images/c1.jpg'} alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" type="submit" label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
														</div>
														<div className='col-sm-1'>
														<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
		 </ul>
		 
		 				 <ul className="paddingLeft12">
					<li>
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb">
									<img src={'images/c2.png'} alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" type="submit" label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
														</div>
														<div className='col-sm-1'>
														<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
		 </ul>
		 
		 
						 <ul className="paddingLeft12">
					<li>
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb">
									<img src={'images/c3.jpg'} alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" type="submit" label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
														</div>
														<div className='col-sm-1'>
														<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
		 </ul> 
		 
		 
		 				 <ul className="paddingLeft12">
					<li>
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb">
									<img src={'images/c4.jpg'} alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" type="submit" label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
														</div>
														<div className='col-sm-1'>
														<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
		 </ul>
		 
		 
		 				 <ul className="paddingLeft12">
					<li>
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb">
									<img src={'images/c5.jpg'} alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" type="submit" label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
														</div>
														<div className='col-sm-1'>
														<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
		 </ul>
		 
		 
		 
		 
		 				 <ul className="paddingLeft12">
					<li>
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb">
									<img src={'images/c6.jpg'} alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" type="submit" label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
														</div>
														<div className='col-sm-1'>
														<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
		 </ul>
		 
		 
		 
		 				 <ul className="paddingLeft12">
					<li>
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb">
									<img src={'images/c7.jpg'} alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" type="submit" label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
														</div>
														<div className='col-sm-1'>
														<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
		 </ul>
		 
		 
		 
		 				 <ul className="paddingLeft12">
					<li>
					<div className="row">
							<div className="col-sm-4 col-md-4">
								<div className="thumb">
									<img src={'images/c8.jpg'} alt="boohoo" ref="cg_image" className="img-responsive"
									/>
								</div>
							</div>
								<div className="col-sm-8 col-md-8 right-content">
									  <div className="name-area">
										<p className="name white textLeft" style={{fontSize: '17px'}}>
										  Cafe mamehico Koen-Dori
										</p>
									  </div>
									  
									  <div className="textLeft">
									  
										<img src={'images/starr.png'}  className="img-responsive" style={{height: '50px'}}/>
									
									  
												<div className="row margin5">
														<div className='col-sm-5'>
														<RaisedButton  className="button-default" type="submit" label="Details" />
														</div>
														<div className='col-sm-5'>
														<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
														</div>
														<div className='col-sm-1'>
														<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
														</div>
														<div className='col-sm-1'>
														</div>
												</div>
									  </div>
								</div>
					</div>	
				</li>
					<div className="underline">	</div>
  </ul>*/}
		 
		
				 
				 
				 {/*<div className='col-sm-6 col-md-6'>
					<img src={'images/h1.jpg'} alt="boohoo" ref="cg_image" className="img-responsive  img-border" 
					onClick={() =>this.props.onMarkerClick(this.getMarker(this.props.title))}
					/>
					<div className="floatLeft">
					
						<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
					
						<p className="margin5">
						30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
						</p>
						<p className="margin5">
						Open all hours! 24 hours a day, 7 days a week
						</p>
				<div className="row">
						<div className='col-sm-5'>
						<RaisedButton  className="button-default" type="submit" label="Details" />
						</div>
						<div className='col-sm-5'>
						<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
						</div>
						<div className='col-sm-1'>
						<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
						</div>
						<div className='col-sm-1'>
						</div>
				</div>	
			</div>
			</div>
			
			<div className='col-sm-6 col-md-6'>
			<img src={'images/h2.jpg'} alt="boohoo" className="img-responsive  img-border"/>
			<div className="floatLeft">
				
				<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
				
				<p className="margin5">
				30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
				</p>
				<p className="margin5">
				Open all hours! 24 hours a day, 7 days a week
				</p>
				<div className="row">
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Details" default />
				</div>
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
				</div>
				<div className='col-sm-1'>
				<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
				</div>
				<div className='col-sm-1'>
				
				</div>
				</div>
			</div>
			</div>
			<div className='col-sm-6'>
			</div>
			</div>
			
				  
				 <div className="row card-heading-centres">
					<div className='col-sm-6 col-md-6'>
					<img src={'images/h1.jpg'} alt="boohoo" ref="cg_image" className="img-responsive  img-border" 
					onClick={() =>this.props.onMarkerClick(this.getMarker(this.props.title))}
					/>
					<div className="floatLeft">
					
						<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
					
						<p className="margin5">
						30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
						</p>
						<p className="margin5">
						Open all hours! 24 hours a day, 7 days a week
						</p>
				<div className="row">
						<div className='col-sm-5'>
						<RaisedButton  className="button-default" type="submit" label="Details" />
						</div>
						<div className='col-sm-5'>
						<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
						</div>
						<div className='col-sm-1'>
						<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
						</div>
						<div className='col-sm-1'>
						</div>
				</div>	
			</div>
			</div>
			
			<div className='col-sm-6 col-md-6'>
			<img src={'images/h2.jpg'} alt="boohoo" className="img-responsive  img-border"/>
			<div className="floatLeft">
				
				<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
				
				<p className="margin5">
				30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
				</p>
				<p className="margin5">
				Open all hours! 24 hours a day, 7 days a week
				</p>
				<div className="row">
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Details" default />
				</div>
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
				</div>
				<div className='col-sm-1'>
				<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
				</div>
				<div className='col-sm-1'>
				
				</div>
				</div>
			</div>
			</div>
			<div className='col-sm-6'>
			</div>
			</div>
				  
				 <div className="row card-heading-centres">
					<div className='col-sm-6 col-md-6'>
					<img src={'images/h1.jpg'} alt="boohoo" ref="cg_image" className="img-responsive  img-border" 
					onClick={() =>this.props.onMarkerClick(this.getMarker(this.props.title))}
					/>
					<div className="floatLeft">
					
						<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
					
						<p className="margin5">
						30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
						</p>
						<p className="margin5">
						Open all hours! 24 hours a day, 7 days a week
						</p>
				<div className="row">
						<div className='col-sm-5'>
						<RaisedButton  className="button-default" type="submit" label="Details" />
						</div>
						<div className='col-sm-5'>
						<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
						</div>
						<div className='col-sm-1'>
						<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
						</div>
						<div className='col-sm-1'>
						</div>
				</div>	
			</div>
			</div>
			
			<div className='col-sm-6 col-md-6'>
			<img src={'images/h2.jpg'} alt="boohoo" className="img-responsive  img-border"/>
			<div className="floatLeft">
				
				<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
				
				<p className="margin5">
				30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
				</p>
				<p className="margin5">
				Open all hours! 24 hours a day, 7 days a week
				</p>
				<div className="row">
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Details" default />
				</div>
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
				</div>
				<div className='col-sm-1'>
				<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
				</div>
				<div className='col-sm-1'>
				
				</div>
				</div>
			</div>
			</div>
			<div className='col-sm-6'>
			</div>
			</div>
					  
				 <div className="row card-heading-centres">
				 
				 
					<div className='col-sm-6 col-md-6'>
					<img src={'images/h1.jpg'} alt="boohoo" ref="cg_image" className="img-responsive  img-border" 
					onClick={() =>this.props.onMarkerClick(this.getMarker(this.props.title))}
					/>
					<div className="floatLeft">
					
						<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
					
						<p className="margin5">
						30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
						</p>
						<p className="margin5">
						Open all hours! 24 hours a day, 7 days a week
						</p>
				<div className="row">
						<div className='col-sm-5'>
						<RaisedButton  className="button-default" type="submit" label="Details" />
						</div>
						<div className='col-sm-5'>
						<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
						</div>
						<div className='col-sm-1'>
						<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
						</div>
						<div className='col-sm-1'>
						</div>
				</div>	
			</div>
			</div>
			
			<div className='col-sm-6 col-md-6'>
			<img src={'images/h2.jpg'} alt="boohoo" className="img-responsive  img-border"/>
			<div className="floatLeft">
				
				<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
				
				<p className="margin5">
				30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
				</p>
				<p className="margin5">
				Open all hours! 24 hours a day, 7 days a week
				</p>
				<div className="row">
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Details" default />
				</div>
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
				</div>
				<div className='col-sm-1'>
				<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
				</div>
				<div className='col-sm-1'>
				
				</div>
				</div>
			</div>
			</div>
			<div className='col-sm-6'>
			</div>
			</div>
			
			
			
			
			
			
			
			
					  
					  
				 <div className="row card-heading-centres">
					<div className='col-sm-6 col-md-6'>
					<img src={'images/h1.jpg'} alt="boohoo" ref="cg_image" className="img-responsive  img-border" 
					onClick={() =>this.props.onMarkerClick(this.getMarker(this.props.title))}
					/>
					<div className="floatLeft">
					
						<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
					
						<p className="margin5">
						30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
						</p>
						<p className="margin5">
						Open all hours! 24 hours a day, 7 days a week
						</p>
				<div className="row">
						<div className='col-sm-5'>
						<RaisedButton  className="button-default" type="submit" label="Details" />
						</div>
						<div className='col-sm-5'>
						<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
						</div>
						<div className='col-sm-1'>
						<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
						</div>
						<div className='col-sm-1'>
						</div>
				</div>	
			</div>
			</div>
			
			<div className='col-sm-6 col-md-6'>
			<img src={'images/h2.jpg'} alt="boohoo" className="img-responsive  img-border"/>
			<div className="floatLeft">
				
				<h4><a href="" title="Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)">Victoria StashPoint - Left Luggage at Hanover Hotel (24/7)</a></h4>
				
				<p className="margin5">
				30 St George's Drive, Pimlico, Victoria, SW1V 4BN, London
				</p>
				<p className="margin5">
				Open all hours! 24 hours a day, 7 days a week
				</p>
				<div className="row">
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Details" default />
				</div>
				<div className='col-sm-5'>
				<RaisedButton type="submit" label="Book" onClick={this.openModal} primary />
				</div>
				<div className='col-sm-1'>
				<span className="glyphicon glyphicon-heart" style={{color:'red'}}></span>
				</div>
				<div className='col-sm-1'>
				
				</div>
				</div>
			</div>
			</div>
			<div className='col-sm-6'>
			</div>*/}
  </div>
			
	
			
			
		</div>			
									
									<div className="col-sm-6 col-md-7">
									<div className="row" style={{height:'100vh'}}>
									
									
											<Map {...props}

											center={this.state.position}
											centerAroundCurrentLocation={true}
											onClick={this.onMapClicked}
											containerStyle={{height:'100%'}}
											>
											
											<Marker position={this.state.position}
												/>
											<Marker
											onClick={this.onMarkerClick}
											title={'Marked by Renu'}
											name={'SOMA'}
											position={{lat: 37.778519, lng: -122.405640}} />
											<Marker
											onClick={this.onMarkerClick}
											title={'Marked by Renu'}
											name={'China Yanji'}
											position={{lat: 43.4970938, lng: 121.9943049}} />
											<Marker
											onClick={this.onMarkerClick}
											title={'Marked by Renu'}
											name={'Indore our place'}
											position={{lat: 22.7195687, lng: 75.85772580000003}}
											
											/>
											<Marker />
											 <InfoWindow
														  marker={this.state.activeMarker}
														  visible={this.state.showingInfoWindow}
														  onClose={this.onInfoWindowClose}>
															<div>
															  <h1>{this.state.selectedPlace.name}</h1>
															</div>
														</InfoWindow>

														<InfoWindow
														  position={{lat: 43.4970938, lng: 121.9943049}}
														  visible={false}>
														  <small>Click on any of the markers to display an additional info.</small>
											</InfoWindow>
											</Map>
										</div>
									</div>
      </div>
    )
  }
})

//export class Container extends React.Component {
	const Container = React.createClass({
	
  
	render: function() {
	  
	  const props = this.props;
    const {google} = this.props;
	  
	    const style = {
      width: '100%',
      height: '100vh'
    }
	const pos = {lat: 37.759703, lng: -122.428093}
    if (!this.props.loaded) {
		console.log("Console Loading...");
      return <div>Loading...</div>
    }
    return (
	<div id="full">
	<Header className="headerstyleSearch" style={{marginTop:'0px'}} />
	<div className="container-fluid">
	<div className="row">
	

	<div className="col-sm-12 col-md-12 card-heading-map" style={{padding:'0px'}}>
	 <div className="map" style={style}>
	  <Map google={google}
	  visible={false}
		className="search-page-style"
		containerStyle={{padding:'0px'}}
	  >
		<Contents {...props} />
		
		</Map>
	  
	  </div> 
	  </div>
	  </div>
	  </div>
	  </div>
    )
  }
})

export default GoogleApiWrapper({
	apiKey: 'AIzaSyBQyFcuS_g7LNEOGk63wXcOYwG9lvRdVAA'
})(Container)