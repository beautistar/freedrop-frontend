import React, { PropTypes } from 'react';
//import SignUpForm from '../components/SignUpForm.jsx';
//import LoginForm from '../components/LoginForm.jsx';
class GooglePage extends React.Component {

  constructor(props, context) {
    super(props, context);
    // set the initial component state

  }
  render() {
	
    return (
	<div>    
	  <h2>you are logged in</h2>
	   
	   <a href={'/logout'}>Log out</a>
	   <a href={'/'}>Go Home</a>
	   
	  </div>
    );
		
  }
  }

GooglePage.contextTypes = {
  router: PropTypes.object.isRequired
};

export default GooglePage;
