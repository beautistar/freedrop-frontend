webpackHotUpdate(0,{

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _headerstyle;

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(339);

	var _Auth = __webpack_require__(398);

	var _Auth2 = _interopRequireDefault(_Auth);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

	var headerstyle = (_headerstyle = {
	  backgroundColor: '#52d3aa',
	  marginTop: '0',
	  backgroundImage: '-webkit-gradient(linear, 0% 0%, 100% 100%, color-stop(0, #3f95ea), color-stop(1, #52d3aa))'
	}, _defineProperty(_headerstyle, 'backgroundImage', '-webkit-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)'), _defineProperty(_headerstyle, 'backgroundImage', 'repeating-linear-gradient(to bottom right, #3f95ea 0%, #52d3aa 100%)'), _defineProperty(_headerstyle, 'backgroundImage', '-ms-repeating-linear-gradient(top left, #3f95ea 0%, #52d3aa 100%)'), _headerstyle);
	var tagPath = {};
	var external = "external";

	var Base = function Base(_ref) {
	  var children = _ref.children;
	  return _react2.default.createElement(
	    'div',
	    null,
	    _react2.default.createElement(
	      'div',
	      { className: 'top-bar' },
	      _react2.default.createElement(
	        'header',
	        { role: 'banner', id: 'fh5co-header', style: "http://" + location.host + '/#/' != location.href ? headerstyle : tagPath },
	        _react2.default.createElement(
	          'div',
	          { className: 'container' },
	          _react2.default.createElement(
	            'nav',
	            { className: 'navbar navbar-default' },
	            _react2.default.createElement(
	              'div',
	              { className: 'navbar-header' },
	              _react2.default.createElement(
	                'a',
	                { href: '#', className: 'js-fh5co-nav-toggle fh5co-nav-toggle', 'data-toggle': 'collapse', 'data-target': '#navbar', 'aria-expanded': 'false', 'aria-controls': 'navbar' },
	                _react2.default.createElement('i', null)
	              ),
	              _react2.default.createElement(
	                'a',
	                { className: 'navbar-brand', href: 'index.html' },
	                'FreeDrop'
	              )
	            ),
	            _react2.default.createElement(
	              'div',
	              { id: 'navbar', className: 'navbar-collapse collapse' },
	              _react2.default.createElement(
	                'ul',
	                { className: 'nav navbar-nav navbar-right' },
	                location.href != "http://" + location.host + '/#/dashboard' ? _react2.default.createElement(
	                  'li',
	                  { className: 'active' },
	                  _react2.default.createElement(
	                    'a',
	                    { href: '#', 'data-nav-section': 'home' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Booking'
	                    )
	                  ),
	                  _react2.default.createElement(
	                    'ul',
	                    { className: 'submenu' },
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        '# of Bag, # of suitcase'
	                      )
	                    ),
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'Time slot for using the service'
	                      )
	                    ),
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'Location (Store)'
	                      )
	                    ),
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'item'
	                      )
	                    )
	                  )
	                ) : '',
	                location.href != "http://" + location.host + '/#/dashboard' ? _react2.default.createElement(
	                  'li',
	                  null,
	                  _react2.default.createElement(
	                    _reactRouter.Link,
	                    { to: '#', 'data-nav-section': 'work' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Promotion Code'
	                    )
	                  )
	                ) : '',
	                location.href != "http://" + location.host + '/#/dashboard' ? _react2.default.createElement(
	                  'li',
	                  null,
	                  _react2.default.createElement(
	                    'a',
	                    { href: '#', 'data-nav-section': 'services' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Calendar'
	                    )
	                  ),
	                  _react2.default.createElement(
	                    'ul',
	                    { className: 'submenu' },
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'Booking'
	                      )
	                    ),
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'History'
	                      )
	                    )
	                  )
	                ) : _react2.default.createElement(
	                  'li',
	                  null,
	                  _react2.default.createElement(
	                    'a',
	                    { href: '#/dashboard', 'data-nav-section': 'services' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Calendar'
	                    )
	                  ),
	                  _react2.default.createElement(
	                    'ul',
	                    { className: 'submenu' },
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'Booking'
	                      )
	                    ),
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'History'
	                      )
	                    )
	                  )
	                ),
	                _Auth2.default.isUserAuthenticated() ? _react2.default.createElement(
	                  'li',
	                  null,
	                  _react2.default.createElement(
	                    _reactRouter.Link,
	                    { to: '/logout' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Log out'
	                    )
	                  ),
	                  ' '
	                ) : _react2.default.createElement(
	                  'li',
	                  null,
	                  _react2.default.createElement(
	                    _reactRouter.Link,
	                    { className: 'external', to: '/login' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Log in'
	                    )
	                  ),
	                  ' '
	                ),
	                _Auth2.default.isUserAuthenticated() ? '' : _react2.default.createElement(
	                  'li',
	                  null,
	                  ' ',
	                  _react2.default.createElement(
	                    _reactRouter.Link,
	                    { className: 'external', to: '/signup' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Register'
	                    )
	                  ),
	                  ' '
	                ),
	                _react2.default.createElement(
	                  'li',
	                  null,
	                  ' ',
	                  _react2.default.createElement(
	                    _reactRouter.Link,
	                    { className: 'external', to: '/search' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Search'
	                    )
	                  ),
	                  ' '
	                ),
	                _Auth2.default.isUserAuthenticated() ? _react2.default.createElement(
	                  'li',
	                  null,
	                  _react2.default.createElement(
	                    _reactRouter.Link,
	                    { to: '#/' },
	                    _react2.default.createElement(
	                      'span',
	                      null,
	                      'Payment Details'
	                    )
	                  ),
	                  _react2.default.createElement(
	                    'ul',
	                    { className: 'submenu' },
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'Stripe'
	                      )
	                    ),
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'Paypal'
	                      )
	                    ),
	                    _react2.default.createElement(
	                      'li',
	                      null,
	                      _react2.default.createElement(
	                        'a',
	                        { href: '#' },
	                        'Credit Card '
	                      )
	                    )
	                  )
	                ) : ''
	              )
	            )
	          )
	        )
	      )
	    ),
	    children
	  );
	};

	Base.propTypes = {
	  children: _react.PropTypes.object.isRequired
	  //path: this.props.location,
	  //path1: location.pathname
	  //path1: this.location.pathname,

	};

	//React.PropTypes.path1 ={location.pathname};

	exports.default = Base;

/***/ })

})