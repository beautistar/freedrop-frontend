const path = require('path');
var webpack = require('webpack');
Iconv = require('iconv').Iconv;

module.exports = {
  // the entry file for the bundle
  entry: path.join(__dirname, '/client/src/app.jsx'),

  // the bundle file we will get in the result
  output: {
    path: path.join(__dirname, '/client/dist/js'),
    filename: 'app.js',
  },

  module: {

    // apply loaders to files that meet given conditions
    loaders: [{
      test: /\.jsx?$/,
      include: path.join(__dirname, '/client/src'),
      loader: 'babel',
      query: {
        presets: ["react", "es2015"],
		 "plugins": [
    ["transform-class-properties", { "spec": true }]
  ]
      },
	  
 

    },
	 {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
          'image-webpack-loader'
        ]
      },
	  {
		  test: /\.json$/,
		  loaders: ['json-loader']
		  
			
	  }
	],
	
	


 plugins: [
  // .... other plugins
  new webpack.NormalModuleReplacementPlugin(
    /\/iconv-loader$/, 'node-noop'
  )
  ],
	




	
  },
   

  // start Webpack in a watch mode, so Webpack will rebuild the bundle on changes
  watch: true
 
    
  
};
